//
//  TKNavigationMenu.h
//
//  Copyright (c) 2013 Telerk Inc. All rights reserved.
//
#import <UIKit/UIKit.h>
typedef enum TKNavigationMenuDisplayMode {
    /**
     Slide menu should push current view on left or right.
     */
    TKNavigationMenuDisplayModePush,
    /**
     Slide menu should be overlay on top of current view.
     */
    TKNavigationMenuDisplayModeOverlay
} TKNavigationMenuDisplayMode;

typedef enum TKNavigationMenuPosition {
    TKNavigationMenuPositionLeft = 0,
    TKNavigationMenuPositionRight = 1
} TKNavigationMenuPosition;

@class TKMenuItem;
@class TKNavigationController;
@protocol TKNavigationMenuDelegate;

@interface TKNavigationMenu : UIView<UITableViewDataSource, UITableViewDelegate>

+ (id)attachToView:(UIView *)view controller:(UIViewController *)viewController;
+ (id)attachToController:(UIViewController *)viewController;

@property (nonatomic) CGFloat size;
@property (nonatomic, copy) NSString* title;
@property (nonatomic) TKNavigationMenuPosition position;
@property (nonatomic) TKNavigationMenuDisplayMode displayMode;
@property (nonatomic, readonly) UIView *hostView;
@property (nonatomic, strong) UIBarButtonItem *barItem;
@property id<TKNavigationMenuDelegate> delegate;

- (void)setHidden:(BOOL)hidden animated:(BOOL)animated;
- (void)move:(UIPanGestureRecognizer *)gesture;

- (NSArray *)items;
- (void)addItem:(TKMenuItem *)newItem;
- (TKMenuItem *)addItemWithTitle:(NSString *)title action:(SEL)selector image:(UIImage *)image header:(NSString *)header;
- (void)removeItem:(TKMenuItem *)anItem;
- (void)removeAllItems;
- (NSInteger)numberOfItems;
- (void)insertItem:(TKMenuItem *)newItem atIndex:(NSInteger)index;
- (NSInteger)indexOfItem:(TKMenuItem *)anObject;

@end

@protocol TKNavigationMenuDelegate <NSObject>

-(void)menuView:(TKNavigationMenu*)menuView didSelectComponent:(TKMenuItem*)item;

@end

@interface UIViewController (TKNavigationMenu)

@property(nonatomic, readonly, retain) TKNavigationMenu *navigationMenu;      // Created on-demand so that a view controller may customize its navigation appearance.

@end

@implementation UIViewController (TKNavigationMenu)

- (TKNavigationMenu *)navigationMenu
{
    return [TKNavigationMenu attachToController:self];
}

@end



