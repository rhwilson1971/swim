//
//  TKChartAnnotationStyle.h
//  TelerikUI
//
//  Copyright (c) 2013 Telerik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TKStyleNode.h"

/**
 The base annotation class
 */
@interface TKChartAnnotationStyle : TKStyleNode

@end
