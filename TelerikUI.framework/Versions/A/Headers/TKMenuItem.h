//
//  TKMenuItem.h
//
//  Copyright (c) 2013 Telerik Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKMenuItem : NSObject

- (id)initWithTitle:(NSString*)title action:(SEL)action;
- (id)initWithTitle:(NSString *)title action:(SEL)action image:(UIImage*)image;
- (id)initWithTitle:(NSString *)title action:(SEL)action image:(UIImage *)image header:(NSString*)header;

@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* header;
@property (nonatomic, strong) UIImage* image;
@property (nonatomic) SEL action;
@property (nonatomic, weak) id tag;

@end
