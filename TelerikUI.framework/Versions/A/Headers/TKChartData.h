//
//  TKChartData.h
//
//  Copyright (c) 2013 Telerik. All rights reserved.

#import <Foundation/Foundation.h>

/**
 * @discussion Represents a data provider protocol for chart's series
 */
@protocol TKChartData <NSObject>

@required

/**
 Returns an x-value in cartesian series. It is used as a value for pie series.
 */
- (id)dataXValue;

/**
 Returns a y-value in cartesian series.
 */
- (id)dataYValue;

@optional

/**
 The name for a pie.
 */
- (NSString *)dataName;

@end
