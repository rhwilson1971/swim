//
//  TKDateTimeAxis.h
//  TelerikUI
//
//  Copyright (c) 2013 Telerik. All rights reserved.
//

#import "TKChartCategoryAxis.h"

/**
 @enum TKDateTimeIntervalUnit determines the label frequency inside TKDateTimeAxis.
 */
typedef enum TKChartDateTimeAxisIntervalUnit {
    /**
     Divides ticks by seconds interval.
     */
    TKChartDateTimeAxisIntervalUnitSeconds,
    /**
     Divides ticks by minutes interval.
     */
    TKChartDateTimeAxisIntervalUnitMinutes,
    /**
     Divided ticks by hours interval.
     */
    TKChartDateTimeAxisIntervalUnitHours,
    /**
     Divides ticks by days interval.
     */
    TKChartDateTimeAxisIntervalUnitDays,
    /**
     Divided ticks by weeks interval.
     */
    TKChartDateTimeAxisIntervalUnitWeeks,
    /**
     Divides ticks by months interval.
     */
    TKChartDateTimeAxisIntervalUnitMonths,
    /**
     Divides ticks by months interval.
     */
    TKChartDateTimeAxisIntervalUnitYears,
    /**
     Divides ticks by custom interval.
     */
    TKChartDateTimeAxisIntervalUnitCustom
   
} TKChartDateTimeAxisIntervalUnit;


/**
 The date-time axis of TKChart.

 <img src="../docs/images/chart-axes-datetime002.png">
 
 @see [Working with Date/Time Axis](chart-axes-datetime)
 
 @discussion The TKChartDateTimeAxis categorical axis is an axis with NSDate values sorted chronologically. It also allows definition of categories based on specific date time components – year, month, day etc. For example, if data values fall in the range of one year, the points can be plotted in categories for each month. If data values fall in the range of one month, the values can be categorized by days. It also introduces several important properties:
 
 - majorTickInterval - Defines an interval among major axis ticks
 
 - baseline - Contains a value, which defines how the series data should be aligned to. (For example: The TKChartBarSeries might render its bars up and down side depending on wether its value is greater or less than the baseline value.)
 
 - offset - Determines a axis value where the axis is crossed with another axis
 */
@interface TKChartDateTimeAxis : TKChartAxis

/**
 Initializes a new axis with minimum and maximum date.
 @param minDate The minimum date that specifies the start of the range.
 @param maxDate The maximum date that specifies the end of the rangee.
 */
- (id)initWithMinimumDate:(NSDate*)minDate andMaximumDate:(NSDate*)maxDate;

/**
 The major tick mark frequency.

 */
@property (nonatomic) NSTimeInterval majorTickInterval;

/**
 The major tick mark frequency using predefined values for a period.

 @discussion The date/time interval units are defined as follows:

    typedef enum {
        TKChartDateTimeAxisIntervalUnitSeconds,     // Divides ticks by seconds interval.
        TKChartDateTimeAxisIntervalUnitMinutes,     // Divides ticks by minutes interval.
        TKChartDateTimeAxisIntervalUnitHours,       // Divided ticks by hours interval.
        TKChartDateTimeAxisIntervalUnitDays,        // Divides ticks by days interval.
        TKChartDateTimeAxisIntervalUnitWeeks,       // Divided ticks by weeks interval.
        TKChartDateTimeAxisIntervalUnitMonths,      // Divides ticks by months interval.
        TKChartDateTimeAxisIntervalUnitYears,       // Divides ticks by months interval.
        TKChartDateTimeAxisIntervalUnitCustom       // Divides ticks by custom interval.
    } TKChartDateTimeAxisIntervalUnit;

 */
@property (nonatomic) TKChartDateTimeAxisIntervalUnit majorTickIntervalUnit;

/**
 The value to which the series data is aligned to.
 */
@property (nonatomic, strong) NSDate *baseline;

/**
 The value where the axis will be crossed by the other.
 */
@property (nonatomic, strong) NSDate *offset;

/**
 The plot mode of the axis.
 @param plotMode The plot mode.
 */
- (void)setPlotMode:(TKChartAxisPlotMode)plotMode;

@end
