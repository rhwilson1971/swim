//
//  MyWeightViewController.h
//  Swim
//
//  Created by Reuben Wilson on 4/10/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyAgeViewControllerDelegate;

@interface MyAgeViewController : UIViewController
@property (assign, nonatomic) id<MyAgeViewControllerDelegate> delegate;
@property double currentAge;

-(IBAction) saveItem:(id)sender;

@end

@protocol MyAgeViewControllerDelegate <NSObject>

- (void)controller:(MyAgeViewController *)controller didSaveAge:(NSInteger)age;

@end