//
//  MySettingsViewController.h
//  Swim
//
//  Created by Reuben Wilson on 3/14/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Profile.h"
#import "SwimUserEntity.h"

@interface MySettingsViewController : UITableViewController
@property (nonatomic,retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,retain) Profile * profile;
@property (nonatomic, weak) SwimUserEntity *swimUser;
@end
