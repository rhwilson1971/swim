//
//  CalorieTableViewController.h
//  Swim
//
//  Created by Reuben Wilson on 3/31/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalorieTableViewController : UITableViewController
@property NSInteger breakFastCalories;
@property NSInteger lunchCalories;
@property NSInteger dinnerCalories;
@property NSInteger snackCalories;

@end
