//
//  Profile.m
//  Swim
//
//  Created by Reuben Wilson on 2/19/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "Profile.h"

@implementation Profile
@synthesize swimUserId;
@synthesize date;
@synthesize age;
@synthesize weight;
@synthesize height;
@synthesize baseHeartRate;
@synthesize measurementId;
@synthesize sex;
@synthesize maxHeartRate;

@end
