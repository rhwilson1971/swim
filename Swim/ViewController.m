//
//  ViewController.m
//  Swim
//
//  Created by Reuben Wilson on 2/21/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "ViewController.h"
#import "WelcomeViewController.h"
#import "NewUserViewController.h"
#import "NewUserStatsTableViewController.h"
#import "LoginViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize viewControllerList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // set colors for the login / sign up buttongs
    
    /*
    
    
    
    _nextViewController = 0;
    
    
    
    viewControllerList = @[@"WelcomeViewController"
                           ,@"NewUserViewController", @"NewUserStatsTableViewController"];
    
    //_pageTitles=@[@"Over 200 Tips and Tricks", @"Discover Hidden Features", @"Help mye lord!", @"Welcome to my home!"];
    //_pageImages=@[@"page1", @"page2", @"page3", @"page4"];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    PageContentViewController *startingViewController = [self viewControllerAtIndex:_nextViewController];
    
    NSArray * viewControllers = @[startingViewController];
    
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)startWalkthrough:(id)sender
{
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    //NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    // NSUInteger index = _nextViewController;
    
    
    if ((_nextViewController == 0) || (_nextViewController == NSNotFound)) {
        return nil;
    }
    
    _nextViewController--;
    
    return [self viewControllerAtIndex:_nextViewController];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    _nextViewController ++;
    
    
    
    // NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    //if (index == NSNotFound) {
    //    return nil;
    //}
    
    //index++;
    //if (index == [self.pageTitles count]) {
    //     return nil;
    //}
    
    //NSUInteger index = _nextViewController;
    
    
    return [self viewControllerAtIndex:_nextViewController];
}

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    //if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
    //    return nil;
    //}
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:[viewControllerList objectAtIndex:index]];
    
    //pageContentViewController.imageFile = self.pageImages[index];
    //pageContentViewController.titleText = self.pageTitles[index];
    //pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

/*
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [viewControllerList count];
}
*/

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}
@end
