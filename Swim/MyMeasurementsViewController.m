//
//  MyMeasurementsViewController.m
//  Swim
//
//  Created by Reuben Wilson on 4/10/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "MyMeasurementsViewController.h"
#import "MyHelpers.h"

@interface MyMeasurementsViewController ()

@property (nonatomic,weak) IBOutlet UITextField *chestSizeField;
@property (nonatomic,weak) IBOutlet UITextField *waistSizeField;
@property (nonatomic,weak) IBOutlet UITextField *hipsSizeField;
@property (nonatomic,weak) IBOutlet UIView *groupView;

@end

@implementation MyMeasurementsViewController
@synthesize chestSizeField;
@synthesize waistSizeField;
@synthesize hipsSizeField;
@synthesize groupView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *defaultText = @"0";
    
    
    groupView.backgroundColor = [MyHelpers swimMediumColor];
    self.view.backgroundColor = [MyHelpers swimLightColor];
    
    chestSizeField.text = defaultText;
    waistSizeField.text = defaultText;
    hipsSizeField.text = defaultText;

    //self.view.backgroundColor = [MyHelpers colorFromHexString:@"01B6AD"];
    // self.navigationController.navigationBar.backgroundColor =[MyHelpers swimDarkColor];
    self.navigationController.navigationBar.tintColor = [MyHelpers swimDarkColor];
    self.navigationController.navigationBar.alpha = 0.7f;
    //self.navigationController.navigationBar.translucent = YES;

}

-(IBAction) saveItem:(id)sender{
    // [self dismissModalViewControllerAnimated:YES];

    //[self dismissViewControllerAnimated:YES completion:<#^(void)completion#>]
    
    double hipsSize = [hipsSizeField.text doubleValue];
    double chestSize = [chestSizeField.text doubleValue];
    double waistSize = [waistSizeField.text doubleValue];

    [self.delegate controller:self didSaveChestSize:chestSize withWaistSize:waistSize withHipsSize:hipsSize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{

}

#pragma mark Seque Actions
-(IBAction)unwindToMySettings:(UIStoryboardSegue*)sender{
    
    NSLog(@"Hi");
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
