//
//  NewUserStatsTableViewController.h
//  Swim
//
//  Created by Reuben Wilson on 1/29/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
#import "LoginModel.h"
#import "Measurement.h"
#import "SwimDataService.h"


@interface NewUserStatsTableViewController : UITableViewController<UITextFieldDelegate,UITableViewDelegate,BSKeyboardControlsDelegate>

@property (nonatomic,retain) IBOutlet UISlider *age;
@property (nonatomic,retain) IBOutlet UISlider *weight;
@property (nonatomic,retain) IBOutlet UISlider *goalWeightSlider;
@property (nonatomic,retain) IBOutlet UIDatePicker *dateOfBirth;
@property (nonatomic,retain) IBOutlet UITextField* chestSize;
@property (nonatomic,retain) IBOutlet UITextField* waistSize;
@property (nonatomic,retain) IBOutlet UITextField* hips;
@property (nonatomic,retain) IBOutlet UITextField *goalWeightText;
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@property (nonatomic,retain) IBOutlet UISegmentedControl *sex;
@property (nonatomic,retain) IBOutlet UITextField *weightText;
@property (nonatomic,retain) IBOutlet UITextField *ageText;
@property (nonatomic,strong) LoginModel * loginModel;
@property (nonatomic,strong) Measurement *measurement;
@property (nonatomic,retain) IBOutlet UISlider *feet;
@property (nonatomic,retain) IBOutlet UISlider *inches;
@property (nonatomic,retain) IBOutlet UITextField *feetText;
@property (nonatomic,retain) IBOutlet UITextField *inchesText;
@property (nonatomic,retain) SwimDataService * dataService;
@property (nonatomic,retain) NSManagedObjectContext *managedObjectContext;

-(IBAction)goalWeightUpdated:(id)sender;
-(IBAction)ageChanged:(id)sender;
-(IBAction)weightChanged:(id)sender;

@end
