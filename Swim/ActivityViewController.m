//
//  ActivityViewController.m
//  Swim
//
//  Created by Reuben Wilson on 1/28/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "ActivityViewController.h"
#include "PNCircleChart.h"

@interface ActivityViewController ()

@end

@implementation ActivityViewController
@synthesize mealsView;
@synthesize exercisesView;
@synthesize exercisesChartLabel;
@synthesize mealsChartLabel;
@synthesize mealsCell;
@synthesize exercisesCell;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // self.connectionManager = [[HxMBLEConnectionManager alloc] init];
    
    
    // Do any additional setup after loading the view.
    
    
    //UILabel * exercisesChartLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 90, exerciseChartWidth, 30)];
    
    // exercises label and char
    
    exercisesChartLabel.text = @"Exercises";
    exercisesChartLabel.textColor = PNFreshGreen;
    exercisesChartLabel.font = [UIFont fontWithName:@"Avenir-Medium" size:23.0];
    exercisesChartLabel.textAlignment = NSTextAlignmentCenter;
    
    CGFloat width = exercisesView.bounds.size.width;
    CGFloat height = exercisesChartLabel.bounds.size.height;
    
    PNCircleChart * exerciseChart = [[PNCircleChart alloc]
                                   initWithFrame:CGRectMake(0, height+10, width, 80.0)
                                   andTotal:[NSNumber numberWithInt:2500]
                                   andCurrent:[NSNumber numberWithInt:1160]];
    
    exerciseChart.backgroundColor = [UIColor clearColor];
    [exerciseChart setStrokeColor:PNGreen];
    [exerciseChart strokeChart];
    
    [exercisesView addSubview:exerciseChart];
    
    
    // meals chart
    width = mealsView.bounds.size.width;
    height = mealsChartLabel.bounds.size.height;
    
    //UILabel * mealsChartLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 90, width, 30)];
    mealsChartLabel.text = @"Meals";
    mealsChartLabel.textColor = PNBlack;
    mealsChartLabel.font = [UIFont fontWithName:@"Avenir-Medium" size:23.0];
    mealsChartLabel.textAlignment = NSTextAlignmentCenter;
    
    PNCircleChart * mealsChart = [[PNCircleChart alloc]
                                   initWithFrame:CGRectMake(0, height+10, width, 80.0)
                                   andTotal:[NSNumber numberWithInt:2500]
                                   andCurrent:[NSNumber numberWithInt:2200]];
    
    mealsChart.backgroundColor = [UIColor clearColor];
    [mealsChart setStrokeColor:PNDarkBlue];
    [mealsChart strokeChart];
    
    [mealsView addSubview:mealsChart];
    
}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait ; //| UIInterfaceOrientationMaskLandscapeLeft;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
