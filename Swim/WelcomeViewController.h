//
//  WelcomeViewController.h
//  Swim
//
//  Created by Reuben Wilson on 1/27/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeViewController : UIViewController
@property (nonatomic,retain) NSManagedObjectContext *managedObjectContext;
@end
