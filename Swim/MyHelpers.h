//
//  MyHelpers.h
//  Swim
//
//  Created by Reuben Wilson on 3/18/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyHelpers : NSObject


+ (UIColor *)colorFromHexString:(NSString *)hexString ;

+ (UIColor *)swimLightColor;
+ (UIColor *)swimMediumColor;
+ (UIColor *)swimDarkColor;

@end
