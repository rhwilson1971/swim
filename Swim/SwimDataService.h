//
//  SwimDataService.h
//  Swim
//
//  Created by Reuben Wilson on 2/19/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginModel.h"
#import "Measurement.h"
#import "Profile.h"




@interface SwimDataService : NSObject

@property (nonatomic, retain) NSString *webServerAddress;

-(SwimDataService *) initWithServerAddress:(NSString*) serverAddress;

-(void)        saveWeight:(NSInteger) swimUserId withWeight:(double) weight onDate:(NSDate *)saveDate;
-(void)        saveHeight:(NSInteger) swimUserId withHeight:(NSInteger) height;
-(void)        saveAge:(NSInteger) swimUserId  withAge:(NSInteger) age;
-(void)        saveMeasurements:(NSInteger) swimUserId withChestSize:(double)chestSize withWaist:(double)waistSize withHips:(double)hipsSize;

-(NSInteger)   saveLogin:(LoginModel*) login;
-(NSInteger)   saveMeasurement:(Measurement *)measurement;
-(NSInteger)   saveGoal:(NSInteger)swim_user_id goalWeight:(float) goalWeight dateStart:(NSDate *)start dateEnd:(NSDate *)end;
-(NSInteger)   saveProfile:(Profile *)profile;
-(NSInteger)   saveHeartrateWithUserId:(NSInteger)swimUserId withHeartrate:(NSInteger) heartrate;
-(NSString*)   saveHeartrateWithUserIdAndReturnInstruction:(NSInteger)swimUserId withSessionId:(NSInteger)sessionid withHeartrate:(NSInteger) heartrate withMaxHeartRate:(NSInteger)maxHeartRate;

-(NSInteger)   saveSessionWithUserId:(NSInteger)userId withExistingSessionId:(NSInteger)sessionId;
-(NSInteger)   saveSessionWithUserId:(NSInteger)userId;

-(LoginModel*) getUser:(NSString *)email withPassword:(NSString *) password;
-(NSArray*)    getMeals;

-(Measurement*) getLastMeasurement:(NSInteger)swimUserId;
-(Profile *)    getProfile:(NSInteger)swimUserId;


//-(Profile *) getProfile

@end
