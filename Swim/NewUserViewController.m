//
//  FNewUserViewController.m
//  Swim
//
//  Created by Reuben Wilson on 1/5/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "NewUserViewController.h"
#import "NewUserStatsTableViewController.h"
#import "MyHelpers.h"
#import "SwimDataService.h"

@interface NewUserViewController ()

@property (nonatomic,retain) IBOutlet UIButton *signUpButton;
@property (nonatomic,retain) IBOutlet UIScrollView * scrollView;

@property (nonatomic,retain) IBOutlet UITextField *email;
@property (nonatomic,retain) IBOutlet UITextField *password;
@property (nonatomic,retain) IBOutlet UITextField *firstName;
@property (nonatomic,retain) IBOutlet UITextField *lastName;
@property (nonatomic,weak) IBOutlet UIView *signUpGroupView;

@property (nonatomic,retain) UIActivityIndicatorView *activityIndicator;

@end

@implementation NewUserViewController

@synthesize firstName;
@synthesize lastName;
@synthesize password;
@synthesize email;
@synthesize activityIndicator;
@synthesize signUpGroupView;
@synthesize scrollView;
@synthesize signUpButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.loginModel = [[LoginModel alloc] init];

    
    NSArray *fields = @[ self.email,
                         self.password,
                         self.firstName,
                         self.lastName];
    
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    
    // scrollView.backgroundColor = [MyHelpers swimLightColor];
    signUpGroupView.backgroundColor = [MyHelpers swimMediumColor];
    
    // standard button attributes
    signUpButton.backgroundColor = [MyHelpers swimLightColor];
    signUpButton.tintColor = [MyHelpers swimDarkColor];
    signUpButton.layer.cornerRadius = 4;
    
    self.title = @"Signup for SWiM";
    
    
    
    self.view.backgroundColor = [MyHelpers swimDarkColor];
}

- (NSUInteger)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait ; //| UIInterfaceOrientationMaskLandscapeLeft;
}

#pragma mark -
#pragma mark Text Field Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.keyboardControls setActiveField:textField];
}

#pragma mark -
#pragma mark Text View Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.keyboardControls setActiveField:textView];
}

#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view = field.superview.superview.superview;
    
    // [self.tableView scrollRectToVisible:view.frame animated:YES];
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark General Methods
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Assume self.view is the table view
    //NSIndexPath *path = [self.tableView indexPathForSelectedRow];
    
    // [segue.destinationViewController setDetail:detail];
    UITextView *view = (UITextView *)[self.view viewWithTag:1001];
    [view resignFirstResponder];
    
    view= (UITextView *)[self.view viewWithTag:1002];
    [view resignFirstResponder];
    
    NSLog(@"Hello");
    
    NSString * sid =[segue identifier];
    
    if([sid isEqualToString:@"SegueNewUserView"]){
    
        NewUserStatsTableViewController *vc = (NewUserStatsTableViewController *)[segue destinationViewController];
    
        vc.managedObjectContext = self.managedObjectContext;
        vc.loginModel = self.loginModel;
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    if( self.password.text.length == 0 || self.firstName.text.length == 0 || self.email.text.length == 0)
    {
        return NO;
    }
    
    BOOL loginSucceeded= [self tryLogin];
    
    return loginSucceeded;
    
}

-(BOOL) tryLogin {
    
    // display activity indicator
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    self.loginModel.FirstName = self.firstName.text;
    self.loginModel.LastName = self.lastName.text;
    self.loginModel.Password = self.password.text;
    
    self.loginModel.email = self.email.text;
    self.loginModel.AuthMethod = UserNamePassword;

    SwimDataService *sds = [[SwimDataService alloc] initWithServerAddress:@"ec2-54-200-147-89.us-west-2.compute.amazonaws.com"];

    NSInteger ret = [sds saveLogin:self.loginModel];

    if( ret > 0 ){
        
        self.loginModel.swimUserId = ret;
        return YES;
    }

    return NO;    
  /*  
    NSString *insertLoginModel =
    [NSString stringWithFormat:@"first_name=%@&last_name=%@&password=%@&email=%@",
     self.loginModel.FirstName,
     self.loginModel.LastName,
     self.loginModel.Password,
     self.loginModel.Email
     ];
    
    NSData * data =[insertLoginModel dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url=[NSURL URLWithString:@"http://54.200.147.89/save_user.php"];
    
    NSMutableURLRequest *request=
    [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    
    NSError *err;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];
    
    NSLog(@"%@:", responseString);
    
    NSString * success = @"success";
    
    [success dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%lu", (unsigned long)responseString.length);
    NSLog(@"%lu", (unsigned long) success.length);
    
    NSError *myError = nil;
    
    NSDictionary *res = [
                         NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&myError];
    
    for(id key in res) {
        
        id value = [res objectForKey:key];
        
        NSString *keyAsString = (NSString *)key;
        NSString *valueAsString = (NSString *)value;
        
        NSLog(@"key: %@", keyAsString);
        NSLog(@"value: %@", valueAsString);
    }
    
    // extract specific value...
    //NSArray *results = [res objectForKey:@"swim_user_id"];
    
    //for (NSDictionary *result in results) {
    //    //NSString *icon = [result objectForKey:@"icon"];
    //    NSLog(@"result: %@", result);
    //}
    
    NSString *item = (NSString *)[res objectForKey:@"swim_user_id"];
    
    self.loginModel.swimUserId = [item intValue];
    */

}

@end
