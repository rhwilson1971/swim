//
//  Reading.m
//  Swim
//
//  Created by Reuben Wilson on 4/22/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "Reading.h"

@implementation Reading

@synthesize maxHeartRate;
@synthesize heartRate;
@synthesize  hrPercentOfMax;
@synthesize  hrPercentOfMaxDelta; // need 2 heart rates
@synthesize  hrThreeReadingAverage;
@synthesize  predictedReading;
@synthesize  targetPercent; // StepInfo
@synthesize  startingHRPercent;
@synthesize  originalTimed;
@synthesize  targetRange3ReadingAvg;
@synthesize  targetRangeRunningAvg;
@synthesize  startingP_ie1;

@synthesize p_lessThanTarget_3ReadingAvg;
@synthesize p_lessThanTarget_runningAvg;
@synthesize  startingP_lessThanTarget;

@synthesize p_greaterThanTarget_3ReadingAvg;
@synthesize  p_greaterThanTarget_runningAvg;

@synthesize  startingP_greaterThanTarget;


@synthesize  h_ie2a;
@synthesize  h_s2a;
@synthesize  h_ToT;

@synthesize  p_ie1;
@synthesize  p_s2;

@synthesize  h_s2b;
@synthesize  h_pow2_s2;

@synthesize  h_ie2b;
@synthesize  h_pow2_ie2;

@synthesize  s1;
@synthesize  e2;
@synthesize  d1a;
@synthesize  d1a_pow2;
@synthesize  t1a;
@synthesize  t1a_pow2;
@synthesize  managementIndex;

@synthesize  pe1;
@synthesize  ps1;
@synthesize  he1;
@synthesize  he1_pow2;
@synthesize  hs1;
@synthesize  hs1_pow2;

@synthesize  d1b;
@synthesize  d1b_pow2;

@synthesize  t1b;
@synthesize  t1b_pow2;

@synthesize  trendIndex;


-(Reading *) initWithMaxHeartRate:(NSInteger) currentHeartRate{
	self = [super init];

	if(nil != self){
		self.maxHeartRate = currentHeartRate;
	}

	return self;
}


@end
