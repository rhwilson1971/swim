//
//  LoginModel.m
//  Swim
//
//  Created by Reuben Wilson on 1/31/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "LoginModel.h"

@implementation LoginModel
@synthesize FirstName;
@synthesize LastName;
@synthesize AuthMethod;
@synthesize Email;
@synthesize Password;
@synthesize Token;
@synthesize swimUserId;
@synthesize sex;

/*
-(LoginModel*) initWithUserName:(NString *)userName password:(NSString *)pass{
    
    LoginModel *model = [[LoginModel alloc] init];
    
    model.UserName = userName;
    model.Password = pass;
    
    return model;
    
}
*/


@end
