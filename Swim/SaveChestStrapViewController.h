//
//  SaveChestStrapViewController.h
//  Swim
//
//  Created by Reuben Wilson on 4/3/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BluetoothDeviceEntity.h"

@interface SaveChestStrapViewController : UIViewController
@property (nonatomic,retain) BluetoothDeviceEntity * bluetoothDevice;


@end
