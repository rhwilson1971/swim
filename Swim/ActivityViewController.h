//
//  ActivityViewController.h
//  Swim
//
//  Created by Reuben Wilson on 1/28/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HxMBLEConnectionManager.h"
@interface ActivityViewController : UITableViewController

@property (nonatomic,retain) IBOutlet UIView * exercisesView;
@property (nonatomic,retain) IBOutlet UIView * mealsView;

@property (nonatomic, retain) IBOutlet UILabel *exercisesChartLabel;
@property (nonatomic, retain) IBOutlet UILabel *mealsChartLabel;

@property (nonatomic, retain) IBOutlet UITableViewCell *exercisesCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *mealsCell;

@property HxMBLEConnectionManager* connectionManager;

@end
