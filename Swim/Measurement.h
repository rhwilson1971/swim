//
//  ProfileModel.h
//  Swim
//
//  Created by Reuben Wilson on 1/31/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginModel.h"

@class LoginModel;

@interface Measurement : NSObject

@property float chest;
@property float waist;
@property float hips;
@property NSInteger skinFold;
@property NSInteger weight;
@property NSInteger heartRate;
@property NSInteger bpSystolic;
@property NSInteger bpDiastolic;
@property NSInteger measurementId;
@property NSInteger swimUserId;

@end
