
// 
// StepInfo.h
// Swim
// Created by Reuben Wilson on 4/9/14.
// Copyright (c) 2014 Thomas Consulting.  All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StepInfo : NSObject

@property NSInteger avgTargetHeartRate;
@property NSInteger minTargetHeartRate;
@property NSInteger maxTargetHeartRate;
@property NSInteger currentStep;
@property NSTimeInterval interval;

-(StepInfo *)initWithAverageRate:(NSInteger) avgTargetHeartRate 
					withMinRate:(NSInteger) minTargetHeartRate 
					withMaxRate:(NSInteger) maxTargetHeartRate 
					withStepNumber:(NSInteger) step
					withInterval:(NSTimeInterval) timeInterval;

@end