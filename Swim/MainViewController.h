//
//  MainViewController.h
//  Swim
//
//  Created by Reuben Wilson on 3/2/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwimUserEntity.h"

typedef NS_ENUM(NSInteger, SwimWorkoutMode) {
    SwimModeWorkout,
    SwimModeMonitor,
    SwimModeNone
};

@interface MainViewController : UIViewController
@property (nonatomic,retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,retain) SwimUserEntity *currentUser;


-(IBAction) start:(id)sender;
-(IBAction) pauseWorkout:(id)sender;
@end
