//
//  BluetoothDeviceEntity.m
//  Swim
//
//  Created by Reuben Wilson on 4/4/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "BluetoothDeviceEntity.h"


@implementation BluetoothDeviceEntity

@dynamic datePaired;
@dynamic deviceId;
@dynamic manufacturer;
@dynamic myName;
@dynamic primaryDevice;
@dynamic uniqueId;

@end
