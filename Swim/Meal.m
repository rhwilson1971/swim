//
//  Meal.m
//  Swim
//
//  Created by Reuben Wilson on 1/31/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "Meal.h"

@implementation Meal
@synthesize meal;
@synthesize mealId;
@synthesize carbs;
@synthesize protein;
@synthesize fats;
@synthesize servingSize;
@synthesize servingSizeWeight;

-(id) initWithJSONData:(NSDictionary*)data{
    self = [super init];
    if(self){
        //NSLog(@"initWithJSONData method called");
        self.mealId = [[data objectForKey:@"id"] integerValue];
        self.meal =  [data objectForKey:@"meal"];
        self.protein = [[data objectForKey:@"protein"] integerValue];
        self.carbs = [[data objectForKey:@"carbs"] integerValue];
        self.fats = [[data objectForKey:@"fats"] integerValue];
        self.servingSize = [[data objectForKey:@"servingSize"] integerValue];
        self.servingSizeWeight = [data objectForKey:@"servingSizeWeight"];
    }
    
    return self;
}


@end
