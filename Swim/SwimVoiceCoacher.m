#import "SwimVoiceCoacher.h"
#import "SwimWorkoutInfo.h"
#import "StepInfo.h"
#import <math.h>
#import "Reading.h"
#import "SwimDataService.h"

@interface SwimVoiceChoacher (){
    NSInteger _maxSteps;
    NSMutableArray * _heartRates;
    NSInteger _currentHeartRate;
    NSInteger _averateHeartRate;
    NSTimeInterval _elapsedTime;
    NSMutableArray * _readings; 
}

@end

@implementation SwimVoiceChoacher
@synthesize workoutInfoSteps;
@synthesize currentStep;
@synthesize maxHeartRate;
@synthesize maxHRSample;

-(SwimVoiceChoacher *)initWithMaxHeartRate:(NSInteger ) maxHR
{
    self = [super init];
    
    if(self){
        self.maxHeartRate = maxHR;
        self.maxHRSample = 3;
        _heartRates = nil;
        _maxSteps = 0;
    }
    
    return self;
}


-(void) initializeReadings{
    _readings = [[NSMutableArray alloc] init];



}

-(void) setupSteps{

    currentStep = 0;

	if ( nil == workoutInfoSteps ){
		workoutInfoSteps = [[NSMutableArray alloc] init];

		SwimWorkoutInfo *  swi1 = [[SwimWorkoutInfo alloc] init];

		swi1.step = 1;
		swi1.interval = 1;
        
        swi1.hrMinFactor = 0.64f;
        swi1.hrMaxFactor = 0.68f;
        swi1.hrAvgFactor = 0.66f;
        
        swi1.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi1.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi1.hrActualMax=maxHeartRate * swi1.hrMaxFactor;
        
        swi1.start = 0;
        swi1.end = 3;
        
        swi1.timeSegment = 3;
        swi1.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi1];
        
        SwimWorkoutInfo * swi2 = [[SwimWorkoutInfo alloc] init];
		swi2.step = 1;
		swi2.interval = 1;
        
        swi2.hrMinFactor = 0.70f;
        swi2.hrMaxFactor = 0.72f;
        swi2.hrAvgFactor = 0.74f;
        
        swi2.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi2.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi2.hrActualMax=maxHeartRate * swi1.hrMaxFactor;
        
        swi2.start = 0;
        swi2.end = 3;
        
        swi2.timeSegment = 3;
        swi2.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi2];
        
        
        SwimWorkoutInfo * swi3 = [[SwimWorkoutInfo alloc] init];
        swi3.step = 1;
		swi3.interval = 1;
        
        swi3.hrMinFactor = 0.64f;
        swi3.hrMaxFactor = 0.68f;
        swi3.hrAvgFactor = 0.66f;
        
        swi3.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi3.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi3.hrActualMax=maxHeartRate * swi1.hrMaxFactor;
        
        swi3.start = 0;
        swi3.end = 3;
        
        swi3.timeSegment = 3;
        swi3.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi3];
        
        SwimWorkoutInfo * swi4 = [[SwimWorkoutInfo alloc] init];
        
        swi4.step = 1;
		swi4.interval = 1;
        
        swi4.hrMinFactor = 0.70f;
        swi4.hrMaxFactor = 0.72f;
        swi4.hrAvgFactor = 0.74f;
        
        swi4.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi4.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi4.hrActualMax=maxHeartRate * swi1.hrMaxFactor;
        
        swi4.start = 0;
        swi4.end = 3;
        
        swi4.timeSegment = 3;
        swi4.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi4];
        
        SwimWorkoutInfo * swi5 = [[SwimWorkoutInfo alloc] init];
        swi5.step = 1;
		swi5.interval = 1;
        
        swi5.hrMinFactor = 0.64f;
        swi5.hrMaxFactor = 0.68f;
        swi5.hrAvgFactor = 0.66f;
        
        swi5.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi5.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi5.hrActualMax=maxHeartRate * swi1.hrMaxFactor;
        
        swi5.start = 0;
        swi5.end = 3;
        
        swi5.timeSegment = 3;
        swi5.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi5];
        
        SwimWorkoutInfo * swi6 = [[SwimWorkoutInfo alloc] init];
        swi6.step = 1;
		swi6.interval = 1;
        
        swi6.hrMinFactor = 0.70f;
        swi6.hrMaxFactor = 0.72f;
        swi6.hrAvgFactor = 0.74f;
    
        swi6.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi6.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi6.hrActualMax=maxHeartRate * swi1.hrMaxFactor;
        
        swi6.start = 0;
        swi6.end = 3;
        
        swi6.timeSegment = 3;
        swi6.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi6];
        
        SwimWorkoutInfo * swi7 = [[SwimWorkoutInfo alloc] init];
        
        swi7.step = 1;
		swi7.interval = 1;
        
        swi7.hrMinFactor = 0.64f;
        swi7.hrMaxFactor = 0.68f;
        swi7.hrAvgFactor = 0.66f;
        
        swi7.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi7.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi7.hrActualMax=maxHeartRate * swi1.hrMaxFactor;
        
        swi7.start = 0;
        swi7.end = 3;
        
        swi7.timeSegment = 3;
        swi7.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi7];
        
        SwimWorkoutInfo * swi8 = [[SwimWorkoutInfo alloc] init];
        swi8.step = 1;
		swi8.interval = 1;
        
        swi8.hrMinFactor = 0.70f;
        swi8.hrMaxFactor = 0.72f;
        swi8.hrAvgFactor = 0.74f;
        
        swi8.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi8.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi8.hrActualMax=maxHeartRate * swi1.hrMaxFactor;
        
        swi8.start = 0;
        swi8.end = 3;
        
        swi8.timeSegment = 3;
        swi8.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi8];
        
        SwimWorkoutInfo * swi9 = [[SwimWorkoutInfo alloc] init];
        
        swi9.step = 1;
		swi9.interval = 1;
        
        swi9.hrMinFactor = 0.64f;
        swi9.hrMaxFactor = 0.68f;
        swi9.hrAvgFactor = 0.66f;
        
        swi9.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi9.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi9.hrActualMax=maxHeartRate * swi1.hrMaxFactor;
        
        swi9.start = 0;
        swi9.end = 3;
        
        swi9.timeSegment = 3;
        swi9.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi9];
        
        SwimWorkoutInfo * swi10 = [[SwimWorkoutInfo alloc] init];
        swi10.step = 1;
		swi10.interval = 1;
        
        swi10.hrMinFactor = 0.70f;
        swi10.hrMaxFactor = 0.72f;
        swi10.hrAvgFactor = 0.74f;
        
        swi10.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi10.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi10.hrActualMax=maxHeartRate * swi1.hrMaxFactor;
        
        swi10.start = 0;
        swi10.end = 3;
        
        swi10.timeSegment = 3;
        swi10.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi10];
        
        SwimWorkoutInfo * swi11 = [[SwimWorkoutInfo alloc] init];
        swi11.step = 1;
		swi11.interval = 1;
        
        swi11.hrMinFactor = 0.64f;
        swi11.hrMaxFactor = 0.68f;
        swi11.hrAvgFactor = 0.66f;
        
        swi11.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi11.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi11.hrActualMax=maxHeartRate * swi1.hrMaxFactor;

        
        swi11.start = 0;
        swi11.end = 3;
        
        swi11.timeSegment = 3;
        swi11.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi11];
        
        SwimWorkoutInfo * swi12 = [[SwimWorkoutInfo alloc] init];
        swi12.step = 1;
		swi12.interval = 1;
        
        swi12.hrMinFactor = 0.70f;
        swi12.hrMaxFactor = 0.72f;
        swi12.hrAvgFactor = 0.74f;
        
        swi12.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi12.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi12.hrActualMax=maxHeartRate * swi1.hrMaxFactor;

        
        swi12.start = 0;
        swi12.end = 3;
        
        swi12.timeSegment = 3;
        swi12.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi12];
        
        SwimWorkoutInfo * swi13 = [[SwimWorkoutInfo alloc] init];
        swi13.step = 1;
		swi13.interval = 1;
        
        swi13.hrMinFactor = 0.64f;
        swi13.hrMaxFactor = 0.68f;
        swi13.hrAvgFactor = 0.66f;
        
        swi13.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi13.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi13.hrActualMax=maxHeartRate * swi1.hrMaxFactor;

        
        swi13.start = 0;
        swi13.end = 3;
        
        swi13.timeSegment = 3;
        swi13.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi13];
        
        SwimWorkoutInfo * swi14 = [[SwimWorkoutInfo alloc] init];
        swi14.step = 1;
		swi14.interval = 1;
        
        swi14.hrMinFactor = 0.70f;
        swi14.hrMaxFactor = 0.72f;
        swi14.hrAvgFactor = 0.74f;
        
        swi14.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi14.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi14.hrActualMax=maxHeartRate * swi1.hrMaxFactor;

        swi14.start = 0;
        swi14.end = 3;
        
        swi14.timeSegment = 3;
        swi14.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi14];
        
        SwimWorkoutInfo * swi15 = [[SwimWorkoutInfo alloc] init];
        swi15.step = 1;
		swi15.interval = 1;
        
        swi15.hrMinFactor = 0.64f;
        swi15.hrMaxFactor = 0.68f;
        swi15.hrAvgFactor = 0.66f;
        
        swi15.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi15.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi15.hrActualMax=maxHeartRate * swi1.hrMaxFactor;

        
        swi15.start = 0;
        swi15.end = 3;
        
        swi15.timeSegment = 3;
        swi15.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi15];
        
        SwimWorkoutInfo * swi16 = [[SwimWorkoutInfo alloc] init];
        swi16.step = 1;
		swi16.interval = 1;
        
        swi16.hrMinFactor = 0.70f;
        swi16.hrMaxFactor = 0.72f;
        swi16.hrAvgFactor = 0.74f;
        
        swi16.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi16.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi16.hrActualMax=maxHeartRate * swi1.hrMaxFactor;

        
        swi16.start = 0;
        swi16.end = 3;
        
        swi16.timeSegment = 3;
        swi16.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi16];
        
        SwimWorkoutInfo * swi17 = [[SwimWorkoutInfo alloc] init];
        swi17.step = 1;
		swi17.interval = 1;
        
        swi17.hrMinFactor = 0.64f;
        swi17.hrMaxFactor = 0.68f;
        swi17.hrAvgFactor = 0.66f;
        
        swi17.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi17.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi17.hrActualMax=maxHeartRate * swi1.hrMaxFactor;

        
        swi17.start = 0;
        swi17.end = 3;
        
        swi17.timeSegment = 3;
        swi17.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi17];
        
        SwimWorkoutInfo * swi18 = [[SwimWorkoutInfo alloc] init];
        swi18.step = 1;
		swi18.interval = 1;
        
        swi18.hrMinFactor = 0.70f;
        swi18.hrMaxFactor = 0.72f;
        swi18.hrAvgFactor = 0.74f;
        
        swi18.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi18.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi18.hrActualMax=maxHeartRate * swi1.hrMaxFactor;

        
        swi18.start = 0;
        swi18.end = 3;
        
        swi18.timeSegment = 3;
        swi18.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi18];
        
        SwimWorkoutInfo * swi19 = [[SwimWorkoutInfo alloc] init];
        swi19.step = 1;
		swi19.interval = 1;
        
        swi19.hrMinFactor = 0.64f;
        swi19.hrMaxFactor = 0.68f;
        swi19.hrAvgFactor = 0.66f;
        
        swi19.hrActualMin=maxHeartRate * swi1.hrMinFactor;
        swi19.hrActualAvg=maxHeartRate * swi1.hrAvgFactor ;
        swi19.hrActualMax=maxHeartRate * swi1.hrMaxFactor;

        
        swi19.start = 0;
        swi19.end = 3;
        
        swi19.timeSegment = 3;
        swi19.timeAccumulated = 3;
        
		[workoutInfoSteps  addObject:swi19];
	}


    _maxSteps = [workoutInfoSteps count];
}

-(NSString *) coachDuringStep:(NSInteger) heartRate{
    return nil;    
}


-(NSTimeInterval) currentStepInterval{

    NSTimeInterval timeInterval=60.0 * 3.0;

    return timeInterval;
}

-(StepInfo*) nextStepWithHeartRate:(NSInteger)heartRate {
    StepInfo * step = nil;
    
    _currentHeartRate = heartRate;
    
    if( nil == _heartRates)
        _heartRates = [[NSMutableArray alloc] initWithCapacity:maxHRSample];
    
    
    if([_heartRates count] < maxHRSample){
        
        [_heartRates addObject:[NSNumber numberWithInteger:maxHRSample]];
    
    }
    else{
    
        NSNumber *first = (NSNumber*) [_heartRates objectAtIndex:1];
        NSNumber *second = (NSNumber *) [_heartRates objectAtIndex:2];
        
        [_heartRates replaceObjectAtIndex:0 withObject:first];
        [_heartRates replaceObjectAtIndex:1 withObject:second];
        [_heartRates replaceObjectAtIndex:2 withObject:[NSNumber numberWithInteger:_currentHeartRate]];

    }

    //_averateHeartRate = [self getAverageHeartRate];
    
    [self addReading:_currentHeartRate];


    return step;
}


-(StepInfo*) nextStep{

    StepInfo *step = nil;

    if( self.currentStep < _maxSteps){

        self.currentStep ++;

        SwimWorkoutInfo * woi=
        [workoutInfoSteps objectAtIndex:self.currentStep];

        NSTimeInterval interval = [self currentStepInterval];
        
        step = [[StepInfo alloc] initWithAverageRate:woi.hrActualAvg withMinRate:woi.hrActualMin
                                         withMaxRate:woi.hrActualMax withStepNumber:currentStep withInterval:interval];
    }

    return step;
}


-(void) resetSteps{
    self.currentStep = 0;
}

-(void) addReading:(NSInteger) heartRate{

}

-(NSString *)instructionWithHeartRate:(NSInteger) heartRate withUserId:(NSInteger)userId withSessionId:(NSInteger)sessionId{

    SwimDataService * dataService = [[SwimDataService alloc] initWithServerAddress:@"ec2-54-200-147-89.us-west-2.compute.amazonaws.com"];
    
    NSString * instruction = [dataService saveHeartrateWithUserIdAndReturnInstruction:userId withSessionId:sessionId withHeartrate:heartRate withMaxHeartRate:maxHeartRate];
    
    return instruction; 
}


@end