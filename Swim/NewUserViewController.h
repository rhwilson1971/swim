//
//  NewUserViewController.h
//  Swim
//
//  Created by Reuben Wilson on 1/5/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginModel.h"
#import "BSKeyboardControls.h"

static NSString * const kClientId = @"464977022001.apps.googleusercontent.com";

@interface NewUserViewController : UIViewController<UITextFieldDelegate,BSKeyboardControlsDelegate>

@property (nonatomic,retain) LoginModel * loginModel;



@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;


@end
