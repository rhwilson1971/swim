//
//  SwimDataService.m
//  Swim
//
//  Created by Reuben Wilson on 2/19/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "SwimDataService.h"
#import "Meal.h"

@implementation SwimDataService
@synthesize webServerAddress;

-(SwimDataService *) initWithServerAddress:(NSString*) serverAddress{
    self = [super init];

    if(self){
        self.webServerAddress = serverAddress;
    }
    
    return self;
}

-(NSString *) getPOSTString:(NSString *)serverAddress withFile:(NSString*) fileName{

    NSString * server = [serverAddress length] > 0 ? serverAddress : @"54.200.147.89";

    return [NSString stringWithFormat:@"http://%@/%@", server, fileName];
}

-(NSInteger) saveLogin:(LoginModel*) login{
   
    NSString *insertLoginModel =
    [NSString stringWithFormat:@"first_name=%@&last_name=%@&password=%@&email=%@",
     login.FirstName,
     login.LastName,
     login.Password,
     login.Email
     ];
    
    NSData * data =[insertLoginModel dataUsingEncoding:NSUTF8StringEncoding];
    NSURL *url=[NSURL URLWithString:[self getPOSTString:self.webServerAddress withFile:@"saveUser.php"]];
    
    NSMutableURLRequest *request=
    [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    
    NSError *err;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];
    
    NSLog(@"%@:", responseString);
    
    NSString * success1 = @"success";
    
    [success1 dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%lu", (unsigned long)responseString.length);
    NSLog(@"%lu", (unsigned long) success1.length);
    
    NSError *myError = nil;
    
    NSDictionary *res = [
                         NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&myError];
    
    NSString *item = (NSString *)[res objectForKey:@"swim_user_id"];

    return [item intValue];    
}

-(NSInteger) saveGoal:(NSInteger)swim_user_id goalWeight:(float)weight dateStart:(NSDate *)start dateEnd:(NSDate *)end
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];

    NSString *dateStringStart = [dateFormatter stringFromDate:start];
    NSString *dateStringEnd = [dateFormatter stringFromDate:end];
    // NSLog(@"%@",dateString);

    NSString *insert =
        [NSString stringWithFormat:@"swim_user_id=%ld&goal_weight=%f&date_start=%@&date_end=%@",
         (long)swim_user_id, weight, dateStringStart, dateStringEnd];
    
    NSData * data = [insert dataUsingEncoding:NSUTF8StringEncoding];

    NSURL *url=[NSURL URLWithString:[self getPOSTString:self.webServerAddress withFile:@"saveGoal.php"]];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    
    NSError * error;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSString * responseString = [NSString stringWithUTF8String:[responseData bytes]];
    NSLog(@"%@:", responseString);
    NSString * success = @"success";
    
    [success dataUsingEncoding:NSUTF8StringEncoding];

    NSError *myError = nil;
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&myError];
    
    NSString *item = (NSString *)[res objectForKey:@"swim_goal_id"];
    
    NSLog(@"id is %@ ", item);
    
    return [item intValue];
}

-(NSInteger) saveMeasurement:(Measurement *)measurement{
    
    NSString *insert =
    [NSString stringWithFormat:@"swim_user_id=%ld&chest_size=%f&waist_size=%f&hips_size=%f&skin_fold=%ld&weight=%ld&heart_rate=%ld&bp_systolic=%ld&bp_diastolic=%ld",
     (long)measurement.swimUserId,
     measurement.chest,
     measurement.waist,
     measurement.hips,
     (long)measurement.skinFold,
     (long)measurement.weight,
     (long)measurement.heartRate,
     (long)measurement.bpSystolic,
     (long)measurement.bpSystolic];
    
    NSData * data =[insert dataUsingEncoding:NSUTF8StringEncoding];
    
    // NSURL *url=[NSURL URLWithString:@"http://54.200.147.89/save_measurement.php"];
    NSURL *url=[NSURL URLWithString:[self getPOSTString: self.webServerAddress withFile:@"saveMeasurement.php"]];

    
    NSMutableURLRequest *request=
    [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    
    NSError *err;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];
    
    NSLog(@"%@:", responseString);
    
    NSString * success = @"success";
    
    [success dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%lu", (unsigned long)responseString.length);
    NSLog(@"%lu", (unsigned long) success.length);
    
    err = nil;
    
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&err];

    NSString *item = (NSString *)[res objectForKey:@"swim_measurement_id"];
    
    return [item intValue];
    
}

-(NSInteger) saveProfile:(Profile *)profile{

    NSString *insert =
    [NSString stringWithFormat:@"swim_user_id=%ld&swim_measurement_id=%ld&height=%ld&weight=%ld&base_heart_rate=%ld&age=%ld&sex=%ld&max_heart_rate=%ld",
     (long)profile.swimUserId,
     (long)profile.measurementId,
     (long)profile.height,
     (long)profile.weight,
     (long)profile.baseHeartRate,
     (long)profile.age,
     (long)profile.sex,
     (long)profile.maxHeartRate ];
    
    NSData * data =[insert dataUsingEncoding:NSUTF8StringEncoding];
    
    // NSURL *url=[NSURL URLWithString:@"http://54.200.147.89/save_profile.php"];
    NSURL *url=[NSURL URLWithString:[self getPOSTString:self.webServerAddress withFile:@"saveProfile.php"]];
    
    NSMutableURLRequest *request=
    [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    
    NSError *err;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];
    
    NSLog(@"%@:", responseString);
    
    NSString * success = @"success";
    
    [success dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%lu", (unsigned long)responseString.length);
    NSLog(@"%lu", (unsigned long) success.length);
    
    err = nil;
    
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&err];
    
    NSString *item = (NSString *)[res objectForKey:@"swim_profile_id"];
    
    return [item intValue];

}

-(LoginModel *)getUser:(NSString *)email withPassword:(NSString *)password{  
    
    NSString *select =
    [NSString stringWithFormat:@"email=%@&password=%@", email, password];
    
    NSData * data =[select dataUsingEncoding:NSUTF8StringEncoding];
    
    // NSURL *url=[NSURL URLWithString:@"http://54.200.147.89/login.php"];
    NSURL *url=[NSURL URLWithString:[self getPOSTString:self.webServerAddress withFile:@"login.php"]];
    
    NSMutableURLRequest *request=
    [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    
    NSError *err;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];
    
    NSLog(@"%@:", responseString);
    
    NSString * success = @"success";
    
    [success dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%lu", (unsigned long)responseString.length);
    NSLog(@"%lu", (unsigned long) success.length);
    
    err = nil;
    
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&err];
    
    NSString *item = (NSString *)[res objectForKey:@"swim_user_id"];
    
    LoginModel * login = NULL;
    
    if( [item intValue] > 0){
        login = [[LoginModel alloc] init];
        login.swimUserId = [item intValue];
        login.FirstName = (NSString *)[res objectForKey:@"first_name"];
        login.LastName = (NSString *)[res objectForKey:@"last_name"];
        login.Email = email;
        login.Password = password;
    }
    
    return  login;
}

-(NSArray *)getMeals {
    
    NSMutableArray * meals = [[NSMutableArray alloc] init];
    
    
    NSString *body = @"";
    
    NSData * data =[body dataUsingEncoding:NSUTF8StringEncoding];
    
    // NSURL *url=[NSURL URLWithString:@"http://54.200.147.89/meals.json"];
    NSURL *url=[NSURL URLWithString:[self getPOSTString:self.webServerAddress withFile:@"meals.json"]];
    
    NSMutableURLRequest *request=
    [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"GET"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    
    NSError *err;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];
    
    NSString * success = @"success";
    
    [success dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%lu", (unsigned long)responseString.length);
    NSLog(@"%lu", (unsigned long) success.length);
    
    err = nil;
    
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
    
    NSArray *results = [res valueForKey:@"meals"];
    
    for (NSDictionary *mealDic in results) {
        
        Meal *meal = [[Meal alloc] initWithJSONData:mealDic];
        
        [meals addObject:meal];
    }
    
    return meals;;
                       
}

-(NSInteger) saveSessionWithUserId:(NSInteger)userId{
    
    return [self saveSessionWithUserId:userId withExistingSessionId:0];

}

-(NSInteger) saveSessionWithUserId:(NSInteger)userId withExistingSessionId:(NSInteger)sessionId{
    
    NSString *insert =
    [NSString stringWithFormat:@"swim_user_id=%ld&swim_session_id=%ld", (long)userId, (long)sessionId];
    
    NSData * data =[insert dataUsingEncoding:NSUTF8StringEncoding];
    
    // NSURL *url=[NSURL URLWithString:@"http://54.200.147.89/save_heartate.php"];
    NSURL *url=[NSURL URLWithString:[self getPOSTString:self.webServerAddress withFile:@"php/swimSession.php"]];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];
    
    NSLog(@"%@:", responseString);
    
    NSString * success = @"success";
    [success dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Response string length %lu bytes", (unsigned long) responseString.length);
    NSLog(@"Success string length %lu bytes", (unsigned long) success.length);
    
    err = nil;
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&err];
    
    NSString *item = (NSString *)[res objectForKey:@"swim_workout_session_id"];
    
    return [item intValue];
}

-(NSString*) saveHeartrateWithUserIdAndReturnInstruction:(NSInteger)swimUserId withSessionId:(NSInteger)sessionid withHeartrate:(NSInteger) heartrate withMaxHeartRate:(NSInteger)maxHeartRate
{
    NSString *insert =
    [NSString stringWithFormat:@"swim_user_id=%ld&heart_rate=%ld&max_heart_rate=%ld&swim_workout_session_id=%ld", (long)swimUserId, (long)heartrate, (long)maxHeartRate, (long)sessionid];
    NSData * data =[insert dataUsingEncoding:NSUTF8StringEncoding];
    
    // NSURL *url=[NSURL URLWithString:@"http://54.200.147.89/save_heartate.php"];
    NSURL *url=[NSURL URLWithString:[self getPOSTString:self.webServerAddress withFile:@"php/SaveheartRateReturnsInstruction.php"]];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];
    
    NSLog(@"%@:", responseString);
    
    NSString * success = @"success";
    [success dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Response string length %lu bytes", (unsigned long) responseString.length);
    NSLog(@"Success string length %lu bytes", (unsigned long) success.length);
    
    err = nil;
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&err];
    
    NSString *item = (NSString *)[res objectForKey:@"instruction"];
    
    return item;
}

-(NSInteger) saveHeartrateWithUserId:(NSInteger)swimUserId withHeartrate:(NSInteger) heartrate{
    
    NSString *insert =
    [NSString stringWithFormat:@"swim_user_id=%ld&heart_rate=%ld", (long)swimUserId, (long)heartrate];
    
    NSData * data =[insert dataUsingEncoding:NSUTF8StringEncoding];

    // NSURL *url=[NSURL URLWithString:@"http://54.200.147.89/save_heartate.php"];
    NSURL *url=[NSURL URLWithString:[self getPOSTString:self.webServerAddress withFile:@"save_heartrate.php"]];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];
    
    NSLog(@"%@:", responseString);
    
    NSString * success = @"success";
    [success dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Response string length %lu bytes", (unsigned long) responseString.length);
    NSLog(@"Success string length %lu bytes", (unsigned long) success.length);
    
    err = nil;
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&err];
    
    NSString *item = (NSString *)[res objectForKey:@"swim_heartrate_id"];

    return [item intValue];
}

-(void) saveWeight:(NSInteger)swimUserId withWeight:(double) weight onDate:(NSDate *)saveDate{
    NSString *insert =
    [NSString stringWithFormat:@"swim_user_id=%ld&weight=%f", (long)swimUserId, weight];
    
    NSData * data =[insert dataUsingEncoding:NSUTF8StringEncoding];

    NSURL *url=[NSURL URLWithString:[self getPOSTString:self.webServerAddress withFile:@"saveWeight.php"]];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];
    
    NSLog(@"%@", responseString);
}

-(void) saveHeight:(NSInteger)swimUserId withHeight:(NSInteger) height{
    NSString *insert =
    [NSString stringWithFormat:@"swim_user_id=%ld&height=%ld", (long)swimUserId, (long)height];
    
    NSData * data =[insert dataUsingEncoding:NSUTF8StringEncoding];

    NSURL *url=[NSURL URLWithString:[self getPOSTString:self.webServerAddress withFile:@"save_height.php"]];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];
    
    NSLog(@"%@", responseString);

}




-(void) saveAge:(NSInteger)swimUserId  withAge:(NSInteger) age{
    NSString *insert =
    [NSString stringWithFormat:@"swim_user_id=%ld&age=%ld", (long)swimUserId, (long)age];
    
    NSData * data =[insert dataUsingEncoding:NSUTF8StringEncoding];

    NSURL *url=[NSURL URLWithString:[self getPOSTString:self.webServerAddress withFile:@"saveHeight.php"]];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];

    NSLog(@"%@", responseString);
}

-(void) saveMeasurements:(NSInteger)swimUserId withChestSize:(double)chestSize withWaist:(double)waistSize withHips:(double)hipsSize{
    NSString *insert =
    [NSString stringWithFormat:@"swim_user_id=%ld&chest=%f&waist=%f&hips=%f", (long)swimUserId, chestSize, waistSize, hipsSize];
    
    NSData * data =[insert dataUsingEncoding:NSUTF8StringEncoding];

    NSURL *url=[NSURL URLWithString:[self getPOSTString:self.webServerAddress withFile:@"saveMeasures.php"]];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];
    
    NSLog(@"%@", responseString);

}

-(Measurement*)getLastMeasurement:(NSInteger)swimUserId{
    Measurement *measurement;
    
    
    return  measurement;
    
}

-(Profile *)getProfile:(NSInteger)swimUserId{
    Profile * profile;
    
    return profile;
}

@end
