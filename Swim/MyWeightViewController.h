//
//  MyWeightViewController.h
//  Swim
//
//  Created by Reuben Wilson on 4/10/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyWeightViewControllerDelegate;

@interface MyWeightViewController : UIViewController
@property (assign, nonatomic) id<MyWeightViewControllerDelegate> delegate;
@property double currentWeight;

-(IBAction) saveItem:(id)sender;

@end

@protocol MyWeightViewControllerDelegate <NSObject>

- (void)controller:(MyWeightViewController *)controller didSaveWeight:(double)weight onDate:(NSDate *)saveDate;

@end