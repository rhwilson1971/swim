//
//  Meal.h
//  Swim
//
//  Created by Reuben Wilson on 1/31/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Meal : NSObject

@property (nonatomic,retain) NSString *meal;
@property NSInteger mealId;
@property NSInteger fats;
@property NSInteger protein;
@property NSInteger carbs;
@property NSInteger servingSize;
@property (nonatomic,retain) NSString *servingSizeWeight;

-(id) initWithJSONData:(NSDictionary*)data;

@end
