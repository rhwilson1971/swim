
// 
// SwimVoiceChoacher.h
// Swim
// Created by Reuben Wilson on 4/4/14.
// Copyright (c) 2014 Thomas Consulting.  All rights reserved.
//

typedef NS_ENUM(NSInteger, TimerIntervals) {
    kMinute = 60,
    kSecond = 61,
    kHour = 62,
    kMilliSecond = 50
};

#define kWorkoutTimerInterval 4 * 60

#import <Foundation/Foundation.h>
#import "StepInfo.h"
@class Reading;

@interface SwimVoiceChoacher : NSObject

@property (nonatomic,retain) NSMutableArray * workoutInfoSteps;
@property NSInteger currentStep;
@property NSInteger maxHeartRate;
@property NSInteger maxHRSample;

-(NSString *)			coachDuringStep:(NSInteger) heartRate;
-(StepInfo *) 			nextStep;
-(StepInfo *) 			nextStepWithHeartRate:(NSInteger) heartRate;
-(SwimVoiceChoacher *) 	initWithMaxHeartRate:(NSInteger ) maxHR;
-(void) 				setupSteps;
-(void) 				resetSteps;
-(NSString *)instructionWithHeartRate:(NSInteger) heartRate withUserId:(NSInteger)userId withSessionId:(NSInteger)sessionId;
                                                  


// -(void)                 addReading:(NSInteger) heartRate;
// -(void)                 calculateReadingParamenters:(Reading *) reading withHeartRate:(NSInteger)heartRate;

@end