//
//  SwimUserEntity.m
//  Swim
//
//  Created by Reuben Wilson on 4/6/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "SwimUserEntity.h"


@implementation SwimUserEntity

@dynamic authenticaionType;
@dynamic email;
@dynamic firstName;
@dynamic lastName;
@dynamic password;
@dynamic swimUserId;
@dynamic isMale;
@dynamic maxHeartRate;

@end
