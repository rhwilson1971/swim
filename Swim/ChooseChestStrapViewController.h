//
//  ChooseChestStrapViewController.h
//  Swim
//
//  Created by Reuben Wilson on 4/3/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "ViewController.h"

@interface ChooseChestStrapViewController : ViewController<UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic,retain) NSMutableArray *chestStraps;
@property (nonatomic,retain) NSString *defaultChestStrap;
@property (nonatomic,retain) NSString *myChestStrapName;

@end
