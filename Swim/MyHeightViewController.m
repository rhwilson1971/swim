//
//  MyHeightViewController.m
//  Swim
//
//  Created by Reuben Wilson on 3/15/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "MyHeightViewController.h"
#import "MyHelpers.h"

@interface MyHeightViewController () <UIPickerViewDelegate,UIPickerViewDelegate>

@property (nonatomic,weak) IBOutlet UIPickerView *heightPicker;
@property (nonatomic,retain) NSMutableArray *feetData;
@property (nonatomic,retain) NSMutableArray *inchesData;

@end

@implementation MyHeightViewController
@synthesize heightPicker;
@synthesize feetData;
@synthesize inchesData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //self.navigationItem.leftBarButtonItem == [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:CancelAction:];
    
    feetData = [[NSMutableArray alloc] init];
    inchesData = [[NSMutableArray alloc] init];
    
    for (int i=1; i<9; i++) {
        
        NSString * feet = [NSString stringWithFormat:@"%d", i];
        
        [feetData addObject:feet];
    }
    
    for (int j=1; j<13; j++){
        NSString *inch = [NSString stringWithFormat:@"%d", j];
        [inchesData addObject:inch];
    }
    self.view.backgroundColor = [MyHelpers swimLightColor];
    self.navigationController.navigationBar.backgroundColor =[MyHelpers swimDarkColor];

}

- (NSUInteger)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait ; //| UIInterfaceOrientationMaskLandscapeLeft;
}


-(IBAction) saveItem:(id)sender{
    // [self dismissModalViewControllerAnimated:YES];

    //[self dismissViewControllerAnimated:YES completion:<#^(void)completion#>]
    
    NSInteger feet = [self.heightPicker selectedRowInComponent:0] + 1;
    NSInteger inches = [self.heightPicker selectedRowInComponent:1] + 1;

    [self.delegate controller:self didSaveHeight:feet withInches:inches];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{
    [self.heightPicker selectRow:4 inComponent:0 animated:NO];
    [self.heightPicker selectRow:6 inComponent:1 animated:NO];
}

#pragma mark Seque Actions
-(IBAction)unwindToMySettings:(UIStoryboardSegue*)sender{
    
    
    NSLog(@"Hi");
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    

}

#pragma mark Picker Datasource methods
-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(component == 0)
        return [feetData count];
    
    return [inchesData count];
}

#pragma mark Picker Delegate methods
-(NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(component == 0)
        return [feetData objectAtIndex:row];
    
    return [inchesData objectAtIndex:row];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
