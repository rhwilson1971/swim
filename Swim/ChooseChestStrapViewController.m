//
//  ChooseChestStrapViewController.m
//  Swim
//
//  Created by Reuben Wilson on 4/3/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "ChooseChestStrapViewController.h"

@interface ChooseChestStrapViewController ()

@end

@implementation ChooseChestStrapViewController
@synthesize chestStraps;
@synthesize defaultChestStrap;
@synthesize myChestStrapName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait ; //| UIInterfaceOrientationMaskLandscapeLeft;
}

#pragma mark - Picker View Delegate Methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return [chestStraps count];
}


#pragma mark - Picker View DataSource Methods



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
