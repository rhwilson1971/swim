// 
// StepInfo.M
// Swim
// Created by Reuben Wilson on 4/9/14.
// Copyright (c) 2014 Thomas Consulting.  All rights reserved.
//

#import "StepInfo.h"

@interface StepInfo ()

@end

@implementation StepInfo
@synthesize avgTargetHeartRate;
@synthesize minTargetHeartRate;
@synthesize maxTargetHeartRate;
@synthesize currentStep;
@synthesize interval;

-(StepInfo *) initWithAverageRate:(NSInteger)avgRate 
                        withMinRate:(NSInteger) minRate 
                        withMaxRate:(NSInteger)maxtRate
                        withStepNumber:(NSInteger)step
                        withInterval:(NSTimeInterval)timeInterval{

    self = [super init];
    
    if(self){    
        self.avgTargetHeartRate = avgRate;
        self.minTargetHeartRate = minRate;
        self.maxTargetHeartRate = maxtRate;
        self.currentStep = step;
        self.interval = timeInterval;
    }
    
    return self;
}

@end