//
//  MySettingsViewController.m
//  Swim
//
//  Created by Reuben Wilson on 3/14/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "MySettingsViewController.h"
#import "MyHeightViewController.h"
#import "MyWeightViewController.h"
#import "MyAgeViewController.h"
#import "MyMeasurementsViewController.h"
#import "MyHelpers.h"
#import "SwimDataService.h"
#import "AppDelegate.h"

@interface MySettingsViewController () <MyHeightViewControllerDelegate,
                                        MyWeightViewControllerDelegate,
                                        MyMeasurementsViewControllerDelegate,
                                        MyAgeViewControllerDelegate>
{
    SwimDataService *_swimDataService;
}
@property NSInteger heightFeet;
@property NSInteger heightInches;

@end

@implementation MySettingsViewController
@synthesize heightFeet;
@synthesize heightInches;
@synthesize managedObjectContext;
@synthesize profile;
@synthesize swimUser;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _swimDataService = [[SwimDataService alloc] initWithServerAddress:@"ec2-54-200-147-89.us-west-2.compute.amazonaws.com"];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.navigationItem setHidesBackButton:false];
    
    self.title = @"Settings";
    
    // self.view.backgroundColor = [MyHelpers colorFromHexString:@"01B6AD"];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.managedObjectContext = appDelegate.managedObjectContext;
    
    self.swimUser = [self getUser];
    
    [self.tableView setBackgroundColor:[MyHelpers swimDarkColor]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait ; //| UIInterfaceOrientationMaskLandscapeLeft;
}

#pragma mark - MyHeightViewController methods

- (void)controller:(MyHeightViewController *)controller didSaveHeight:(NSInteger)feet withInches:(NSInteger)inches{
    // Dismiss the modal view controller
    
    NSInteger height = inches + feet * 12;

    if ( nil != self.swimUser) {
        [_swimDataService saveHeight:[swimUser.swimUserId intValue] withHeight:height];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
 }

 - (void)controller:(MyWeightViewController *)controller didSaveWeight:(double)weight onDate:(NSDate*) saveDate{
    // Dismiss the modal view controller
    
     if( nil != self.swimUser){
         [_swimDataService saveWeight:[self.swimUser.swimUserId intValue] withWeight:weight onDate:saveDate];
     }
    [self dismissViewControllerAnimated:YES completion:nil];
    
 }


- (void)controller:(MyMeasurementsViewController *)controller didSaveChestSize:(double)chest withWaistSize:(double)waistSize withHipsSize:(double)hipsSize {
    // Dismiss the modal view controller
    
    if( nil != self.swimUser) {
        [_swimDataService saveMeasurements:[self.swimUser.swimUserId intValue] withChestSize:chest withWaist:waistSize withHips:hipsSize];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
 }


- (void)controller:(MyAgeViewController *)controller didSaveAge:(NSInteger)age{
    // Dismiss the modal view controller
    
    if( nil == self.swimUser){
        [_swimDataService saveAge:[self.swimUser.swimUserId intValue] withAge:age];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
 }


#pragma mark - Table view data source

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}
*/

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"segueToHeightSettingsView"]) {
        // Prepare the settings view where the user inputs the 'serviceType' and local peer 'displayName'
    
        UINavigationController *vc = segue.destinationViewController;
        
        MyHeightViewController *heightViewController = (MyHeightViewController *)vc.topViewController;
        
        heightViewController.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"segueToWeightSettingsView"]){

        UINavigationController *vc = segue.destinationViewController;
        
        MyWeightViewController *weightViewController = (MyWeightViewController *)vc.topViewController;
        
        weightViewController.delegate = self;

    }
    else if([segue.identifier isEqualToString:@"segueToAgeSettingsView"]){
        
        UINavigationController *vcNav = segue.destinationViewController;
        
        MyAgeViewController *vc = (MyAgeViewController *)vcNav.topViewController;
        
        vc.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"segueToVitalStatsView"]){
        
        UINavigationController *vcNav = segue.destinationViewController;
        
        MyMeasurementsViewController *vc = (MyMeasurementsViewController *)vcNav.topViewController;
        
        vc.delegate = self;
    }


    
}

-(SwimUserEntity *) getUser{
    
    //SwimUserEntity *swimUser = nil;
    
    NSManagedObjectContext * moc = self.managedObjectContext;
    
    if(nil == moc) return swimUser;
    
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"SwimUserEntity" inManagedObjectContext:moc];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entityDescription];
    NSError *error;
    
    NSArray *items = [moc executeFetchRequest:request error:&error];
    
    if(nil != items && [items count] > 0)
    {
        swimUser = [items objectAtIndex:0];
    }
    
    return swimUser;
}

@end
