//
//  ViewController.h
//  Swim
//
//  Created by Reuben Wilson on 2/21/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"

@interface ViewController : UINavigationController<UIPageViewControllerDataSource>


-(IBAction)startWalkthrough:(id)sender;
@property (strong, nonatomic) UIPageViewController *pageViewController;
//@property (strong, nonatomic) NSArray *pageTitles;
//@property (strong, nonatomic) NSArray *pageImages;
@property NSInteger nextViewController;
@property (strong, nonatomic) NSArray *viewControllerList;

@end
