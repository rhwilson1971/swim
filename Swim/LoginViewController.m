//
//  LoginViewController
//  Swim
//
//  Created by Reuben Wilson on 1/5/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "LoginViewController.h"
#import "SwimDataService.h"
#import "SwimUserEntity.h"
#import "MyHelpers.h"

@interface LoginViewController ()
@property (nonatomic,weak) IBOutlet UITextField *password;
@property (nonatomic,weak) IBOutlet UITextField *email;
@property (nonatomic,weak) IBOutlet UIButton *loginButton;
@property (nonatomic,weak) IBOutlet UILabel *errorMessageLabel;
@property (nonatomic,weak) IBOutlet UIView * loginGroupView;

@end

@implementation LoginViewController
@synthesize password;
@synthesize email;
@synthesize managedObjectContext;
@synthesize loginButton;
@synthesize errorMessageLabel;
@synthesize loginGroupView;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    loginGroupView.backgroundColor = [MyHelpers swimMediumColor];
    
    loginButton.layer.cornerRadius = 3;
    loginButton.layer.borderWidth = 1;
    loginButton.layer.borderColor = [MyHelpers swimLightColor].CGColor;
    
    loginButton.backgroundColor = [MyHelpers swimLightColor];
    
    self.view.backgroundColor = [MyHelpers swimDarkColor];
    
    errorMessageLabel.hidden = YES;
}

- (NSUInteger)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait ; //| UIInterfaceOrientationMaskLandscapeLeft;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) validateUser{
    
    NSString * email1 = self.email.text;
    NSString * password1 = self.password.text;
    
    SwimDataService * dataService = [[SwimDataService alloc] init];
    
    LoginModel * login = [dataService getUser:email1 withPassword:password1];
    
    if( NULL == login){
        errorMessageLabel.hidden = NO;
        errorMessageLabel.tintColor = [UIColor redColor];
        return NO;
    }
    else {
        
        NSManagedObjectContext *moc = [self managedObjectContext];
        
        NSError *error = nil;
        
        SwimUserEntity *swimUserEntity = (SwimUserEntity *)[NSEntityDescription insertNewObjectForEntityForName:@"SwimUserEntity" inManagedObjectContext:moc];
        
        swimUserEntity.firstName = login.FirstName;
        swimUserEntity.lastName = login.LastName;
        swimUserEntity.email = login.Email;
        swimUserEntity.password = login.Password;
        swimUserEntity.isMale = [NSNumber numberWithInteger:login.sex];
        
        swimUserEntity.swimUserId=[[NSNumber alloc] initWithLong:login.swimUserId];
        
        [moc save:&error];
        
        if(nil != error)
        {
            NSLog(@"Error occurred saving prayer request");
        }
        
        NSLog(@"Saved user info");
        
        return YES;
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    if( [self validateUser]){
        return YES;
    }
    
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"LoginToMainViewSegue"]) {
        NSLog(@"seque to main tab view");
    }
}

@end
