//
//  MainViewTabController.h
//  Swim
//
//  Created by Reuben Wilson on 3/6/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwimUserEntity.h"

@interface MainViewTabController : UITabBarController
@property (nonatomic,retain)  NSManagedObjectContext * managedObjectContext;
@property (nonatomic) SwimUserEntity *swimUser;
@end
