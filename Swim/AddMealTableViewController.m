//
//  AddMealTableViewController.m
//  Swim
//
//  Created by Reuben Wilson on 4/1/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "AddMealTableViewController.h"
#import "Meal.h"
#import "SwimDataService.h"

@interface AddMealTableViewController ()
@property (nonatomic,retain) NSMutableArray *meals;
@property (nonatomic,retain) SwimDataService *dataService;
@property (nonatomic,retain) NSMutableData* downloadData;
@property (nonatomic,retain) NSURLConnection *downloadConnection;
@end

@implementation AddMealTableViewController
@synthesize meals;
@synthesize dataService;

-(IBAction)save:(id)sender{
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    meals = [[NSMutableArray alloc] init ]; //]initWithObjects:@"Atkins Shake", @"Atkins Shake 2", @"Atkins Shake 3", nil];
    
    self.dataService = [[SwimDataService alloc] init];
    
    meals = [[NSMutableArray alloc] initWithArray:[self.dataService getMeals]];
    
}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait ; //| UIInterfaceOrientationMaskLandscapeLeft;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
// #warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [meals count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellId=@"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
        
    }
    
    Meal *meal = [meals objectAtIndex:[indexPath row]];
    
    cell.textLabel.text = meal.meal;
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setupMealsFromJSONArray:(NSData*)dataFromServerArray{
    NSError *error = nil;
    NSMutableArray *placesArray = [[NSMutableArray alloc] init];
    NSArray *arrayFromServer = [NSJSONSerialization JSONObjectWithData:dataFromServerArray options:0 error:&error];
    
    
    if(error){
        NSLog(@"error parsing the json data from server with error description - %@", [error localizedDescription]);
    }
    else {
        placesArray = [[NSMutableArray alloc] init];
        for(NSDictionary *eachMeal in arrayFromServer)
        {
            Meal *meal = [[Meal alloc] initWithJSONData:eachMeal];
            [meals addObject:meal];
        }
        
        //Now you have your placesArray filled up with all your data objects
    }
}


-(void)startDownload {
    NSURL* jsonURL = [NSURL URLWithString:@"http://54.200.147.89/meals.json"];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:jsonURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
    
    _downloadData = [NSMutableData dataWithCapacity:512];
    
    _downloadConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    
}

@end
