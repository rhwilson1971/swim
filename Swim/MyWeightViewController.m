//
//  MyWeightViewController.m
//  Swim
//
//  Created by Reuben Wilson on 4/10/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "MyWeightViewController.h"
#import "MyHelpers.h"

@interface MyWeightViewController () <UIPickerViewDelegate>

@property (nonatomic,retain) IBOutlet UITextField  *weightField;
@property (nonatomic,retain) IBOutlet UIDatePicker *datePicker;

@end

@implementation MyWeightViewController
@synthesize currentWeight;
@synthesize weightField;
@synthesize datePicker;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    if(currentWeight == 0 )
        currentWeight = 200.0;

    weightField.text = [NSString stringWithFormat:@"%f", currentWeight];
    
    //self.navigationItem.leftBarButtonItem == [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:CancelAction:];
    //[self.datePicker datePickerMode:UIDatePickerModeDate];
    
    
    
    //[datePicker datePickerMode:UIDatePickerModeDate];
    self.view.backgroundColor = [MyHelpers swimLightColor];
    self.navigationController.navigationBar.backgroundColor =[MyHelpers swimDarkColor];
    


}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait ; //| UIInterfaceOrientationMaskLandscapeLeft;
}

-(IBAction) saveItem:(id)sender{
    //[self dismissModalViewControllerAnimated:YES];

    // [self dismissViewControllerAnimated:YES completion:<#^(void)completion#>]

    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.delegate controller:self didSaveWeight:[weightField.text intValue] onDate:[datePicker date]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{
    
    // [self.datePicker ]
}

#pragma mark Seque Actions
-(IBAction)unwindToMySettings:(UIStoryboardSegue*)sender{
    
    
    NSLog(@"Hi");
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    

}

#pragma mark Picker Datasource methods

#pragma mark Picker Delegate methods



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
