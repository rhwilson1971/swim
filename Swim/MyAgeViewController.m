//
//  MyWeightViewController.m
//  Swim
//
//  Created by Reuben Wilson on 4/10/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "MyAgeViewController.h"
#import "MyHelpers.h"

@interface MyAgeViewController () <UIPickerViewDelegate,UIPickerViewDelegate>

@property (nonatomic,weak) IBOutlet UIPickerView *agePicker;
@property (nonatomic,retain) NSMutableArray *ageData;

@end

@implementation MyAgeViewController
@synthesize currentAge;
@synthesize ageData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    if(currentAge == 0 )
        currentAge = 25;
    
    //self.navigationItem.leftBarButtonItem == [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:CancelAction:];

    self.ageData = [[NSMutableArray alloc] init];
    
    for (int i=18; i<120; i++) {
        
        NSString * age = [NSString stringWithFormat:@"%d", i];
        
        [ageData addObject:age];
    }
    self.view.backgroundColor = [MyHelpers swimLightColor];
    self.navigationController.navigationBar.backgroundColor =[MyHelpers swimDarkColor];
    

}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait ; //| UIInterfaceOrientationMaskLandscapeLeft;
}

-(IBAction) saveItem:(id)sender{
    // [self dismissModalViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:<#^(void)completion#>]

    NSInteger selectedAge = [_agePicker selectedRowInComponent:0];
    
    if( -1 == selectedAge )
        selectedAge = currentAge;

    [self.delegate controller:self didSaveAge:selectedAge];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{
    [self.agePicker selectRow:currentAge inComponent:0 animated:NO];

}

#pragma mark Seque Actions
-(IBAction)unwindToMySettings:(UIStoryboardSegue*)sender{
    
    
    NSLog(@"Hi");
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    

}

#pragma mark Picker Datasource methods
-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return [ageData count];
}

#pragma mark Picker Delegate methods
-(NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    return [ageData objectAtIndex:row];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
