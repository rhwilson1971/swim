//
//  MyMeasurementsViewController.h
//  Swim
//
//  Created by Reuben Wilson on 4/10/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyMeasurementsViewControllerDelegate;

@interface MyMeasurementsViewController : UIViewController
@property (assign, nonatomic) id<MyMeasurementsViewControllerDelegate> delegate;

-(IBAction) saveItem:(id)sender;

@end

@protocol MyMeasurementsViewControllerDelegate <NSObject>

- (void)controller:(MyMeasurementsViewController *)controller didSaveChestSize:(double)chest withWaistSize:(double)waistSize withHipsSize:(double)hipsSize;

@end