//
//  MyHeightViewController.h
//  Swim
//
//  Created by Reuben Wilson on 3/15/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyHeightViewControllerDelegate;

@interface MyHeightViewController : UIViewController
@property (assign, nonatomic) id<MyHeightViewControllerDelegate> delegate;

-(IBAction) saveItem:(id)sender;

@end

@protocol MyHeightViewControllerDelegate <NSObject>

- (void)controller:(MyHeightViewController *)controller didSaveHeight:(NSInteger)feet withInches:(NSInteger)inches;

@end