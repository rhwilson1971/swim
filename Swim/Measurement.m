//
//  ProfileModel.m
//  Swim
//
//  Created by Reuben Wilson on 1/31/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "Measurement.h"

@implementation Measurement
@synthesize chest;
@synthesize waist;
@synthesize hips;
@synthesize skinFold;
@synthesize weight;
@synthesize heartRate;
@synthesize bpSystolic;
@synthesize bpDiastolic;
@synthesize swimUserId;
@synthesize measurementId;
@end
