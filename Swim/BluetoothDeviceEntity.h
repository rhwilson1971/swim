//
//  BluetoothDeviceEntity.h
//  Swim
//
//  Created by Reuben Wilson on 4/4/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface BluetoothDeviceEntity : NSManagedObject

@property (nonatomic, retain) NSDate * datePaired;
@property (nonatomic, retain) NSString * deviceId;
@property (nonatomic, retain) NSString * manufacturer;
@property (nonatomic, retain) NSString * myName;
@property (nonatomic, retain) NSNumber * primaryDevice;
@property (nonatomic, retain) NSString * uniqueId;

@end
