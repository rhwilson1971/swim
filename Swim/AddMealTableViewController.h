//
//  AddMealTableViewController.h
//  Swim
//
//  Created by Reuben Wilson on 4/1/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MealType){
    kBreakfast,
    kLunch,
    kDinner,
    kSnack
};

@interface AddMealTableViewController : UITableViewController


@property MealType mealType;

-(IBAction)save:(id)sender;


@end
