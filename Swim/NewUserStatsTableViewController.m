//
//  NewUserStatsTableViewController.m
//  Swim
//
//  Created by Reuben Wilson on 1/29/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "NewUserStatsTableViewController.h"
#import "AppDelegate.h"
#import "SwimUserEntity.h"
#import "math.h"
#import "MyHelpers.h"

@interface NewUserStatsTableViewController ()
{
    NSManagedObjectContext *managedObjectContext;
}
@property NSInteger maxHeartRate;
@property (nonatomic,retain) IBOutlet UIButton *doneButton;
@end

@implementation NewUserStatsTableViewController
@synthesize chestSize;
@synthesize waistSize;
@synthesize hips;
@synthesize dateOfBirth;
@synthesize goalWeightSlider;
@synthesize goalWeightText;
@synthesize sex;
@synthesize weight;
@synthesize age;
@synthesize weightText  ;
@synthesize ageText;
@synthesize feet;
@synthesize inches;
@synthesize feetText;
@synthesize inchesText;
@synthesize measurement;
@synthesize dataService;
@synthesize managedObjectContext;

#pragma mark -
#pragma mark - initialization
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    feet.minimumValue = 2;
    feet.maximumValue = 10;
    feet.value = 5;
    
    inches.minimumValue = 0;
    inches.maximumValue = 12;
    inches.value = 7;
    
    age.minimumValue = 18;
    age.maximumValue = 120;
    
    age.value = 30;
    
    weight.minimumValue = 70;
    weight.maximumValue = 400;
    weight.value = 150;
    
    sex.selectedSegmentIndex = 0;
    
    NSArray *fields = @[ self.ageText,
                         self.weightText,
                         self.goalWeightText,
                         self.chestSize,
                         self.waistSize,
                         self.hips,
                         self.feetText,
                         self.inchesText];
    
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    goalWeightSlider.minimumValue = 70;
    goalWeightSlider.maximumValue = 400;
    goalWeightSlider.value = 150;
 
    ageText.text = [NSString stringWithFormat:@"%d", (int)age.value];
    weightText.text = [NSString stringWithFormat:@"%d", (int)weight.value];
    goalWeightText.text = [NSString stringWithFormat:@"%d", (int)goalWeightSlider.value];
    feetText.text = [NSString stringWithFormat:@"%d", (int)feet.value];
    inchesText.text = [NSString stringWithFormat:@"%d", (int)inches.value];
    
    self.measurement = [[Measurement alloc] init];
    self.dataService = [[SwimDataService alloc] initWithServerAddress:@"ec2-54-200-147-89.us-west-2.compute.amazonaws.com"];
    
    // self.view.backgroundColor = [MyHelpers swimLightColor];
    [self.tableView setBackgroundColor:[MyHelpers swimLightColor]];
    
    //signUpButton.backgroundColor = [MyHelpers swimDarkColor];
    //signUpButton.tintColor = [MyHelpers swimLightColor];
    _doneButton.tintColor = [MyHelpers swimLightColor];
    _doneButton.backgroundColor = [MyHelpers swimDarkColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
#pragma mark -
#pragma mark Text Field Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.keyboardControls setActiveField:textField];
}

#pragma mark -
#pragma mark Text View Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.keyboardControls setActiveField:textView];
}

#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    view = field.superview.superview.superview;
    [self.tableView scrollRectToVisible:view.frame animated:YES];
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

-(IBAction)goalWeightUpdated:(id)sender{
    UISlider *slider = (UISlider *)sender;
    
    int weightAsInt = (int)slider.value + 0.5;
    
    NSString *newText = [[NSString alloc] initWithFormat:@"%d lbs",
                         weightAsInt];
    
    goalWeightText.text = newText;
}

-(IBAction)ageChanged:(id)sender{
    UISlider *slider = (UISlider *)sender;
    
    int ageAsInt = (int)(slider.value + 0.5f);
    
    NSString *newText = [[NSString alloc] initWithFormat:@"%d",
                         ageAsInt];
    
    ageText.text = newText;
}

-(IBAction)weightChanged:(id)sender{
    UISlider *slider = (UISlider *)sender;
    
    int weightAsInt = (int)slider.value + 0.5;
    
    NSString *newText = [[NSString alloc] initWithFormat:@"%d",
                         weightAsInt];
    
    weightText.text = newText;
}

-(IBAction)feetChanged:(id)sender{
    UISlider *slider = (UISlider *)sender;
    
    int feetAsInt = (int)slider.value + 0.5;
    
    NSString *newText = [[NSString alloc] initWithFormat:@"%d",
                         feetAsInt];
    
    feetText.text = newText;
}

-(IBAction)inchesChanged:(id)sender{
    UISlider *slider = (UISlider *)sender;
    
    int inchesAsInt = (int)slider.value + 0.5;
    
    NSString *newText = [[NSString alloc] initWithFormat:@"%d",
                         inchesAsInt];
    
    inchesText.text = newText;
}
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}*/

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    [self saveStats];
    
    return YES;
}

#pragma mark - Business Logic
-(NSInteger) calculateMaxHeartRate:(NSInteger)myAge withHeight:(NSInteger)height withWeight:(NSInteger)weight withSex:(NSInteger)mySex{
    
    const NSInteger maleRefWeight = 206;
    const NSInteger femaleRefWeight = 191.5;
    
    NSInteger myHeartRate = 0;
    
    if( mySex == 0) // male
        myHeartRate = maleRefWeight - (0.88 * myAge);
    else // 1 female
        myHeartRate = femaleRefWeight - ( 0.007 * (pow(myAge, 2)));
    
    return myHeartRate;
}

-(BOOL) saveStats
{
    NSInteger height =
    12 * feet.value + inches.value;
    
    // 0 male 1 female
    NSInteger sexValue = sex.selectedSegmentIndex;;
    
    self.maxHeartRate = [self calculateMaxHeartRate:age.value withHeight:height withWeight:weight.value withSex:sexValue];
    
    self.measurement.swimUserId = self.loginModel.swimUserId;
    [self.measurement setWaist:[waistSize.text intValue]];
    [self.measurement setChest:[chestSize.text intValue]];
    [self.measurement setHeartRate:self.maxHeartRate];
    [self.measurement setHips:[hips.text floatValue]];
    [self.measurement setWeight:weight.value];
    [self.measurement setBpDiastolic:0];
    [self.measurement setBpSystolic:0];
    
    // save measurement
    NSInteger mid = [dataService saveMeasurement:self.measurement];
    
    Profile *profile = [[Profile alloc] init];

    profile.height = 12 * feet.value + inches.value;
    profile.age = age.value;
    profile.measurementId = mid;
    profile.swimUserId = self.loginModel.swimUserId;
    profile.baseHeartRate = measurement.heartRate;
    profile.weight = measurement.weight;
    profile.sex = sexValue;
    
    profile.maxHeartRate = self.maxHeartRate;
    
    // save a profile
    [dataService saveProfile:profile];
    
    NSDate *dateStart = [NSDate date];
    NSDate *dateEnd = [NSDate date];
    
    // save a goal
    [dataService saveGoal:self.measurement.swimUserId goalWeight:measurement.weight dateStart:dateStart dateEnd:dateEnd];
    
    NSManagedObjectContext *moc = [self managedObjectContext];
    
    NSError *error = nil;
    
    SwimUserEntity *swimUserEntity = (SwimUserEntity *)[NSEntityDescription insertNewObjectForEntityForName:@"SwimUserEntity" inManagedObjectContext:moc];
    
    swimUserEntity.firstName    = self.loginModel.FirstName;
    swimUserEntity.lastName     = self.loginModel.LastName;
    swimUserEntity.email        = self.loginModel.Email;
    swimUserEntity.password     = self.loginModel.Password;
    swimUserEntity.isMale       = [NSNumber numberWithInteger:profile.sex];
    swimUserEntity.maxHeartRate = [NSNumber numberWithInteger:profile.maxHeartRate];
    swimUserEntity.swimUserId   = [[NSNumber alloc] initWithLong:self.loginModel.swimUserId];
    
    // save entity
    [moc save:&error];
    
    if(nil != error)
    {
        NSLog(@"Error occurred saving prayer request");
    }
    
	NSLog(@"Save button pressed");
    
    return YES;
    
}
@end
