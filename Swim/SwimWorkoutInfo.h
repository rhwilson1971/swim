#import <Foundation/Foundation.h>

@interface SwimWorkoutInfo : NSObject

@property NSInteger step;
@property NSInteger interval;
@property float hrMinFactor;
@property float hrMaxFactor;
@property float hrAvgFactor;
@property NSInteger hrActualMin;
@property NSInteger hrActualMax;
@property NSInteger hrActualAvg;
@property NSInteger start;
@property NSInteger end ;
@property NSInteger timeSegment;
@property NSInteger timeAccumulated;

//-(void) initSwimWorkoutInfo:(NSInteger)step interval:(NSInteger)interval hrMinFactor:(NSInteger)hrMinFactor;


@end


