//
//  MainViewTabController.m
//  Swim
//
//  Created by Reuben Wilson on 3/6/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "MainViewTabController.h"
#import "MainViewController.h"
#import "MySettingsViewController.h"
#import "MyHelpers.h"

@interface MainViewTabController ()

@end

@implementation MainViewTabController
@synthesize managedObjectContext;
@synthesize swimUser;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[self.navigationItem setHidesBackButton:YES animated:YES];
    // Do any additional setup after loading the view.

    // hide the navigation button on the main tab view controller when
    // it displays the view in tab
    [self.navigationItem setHidesBackButton:YES animated:NO];
    
    
    //mainView.managedObjectContext = self.managedObjectContext;
    //for( UIViewController *v in self.viewControllers)
    
    for (UIViewController *v in self.tabBarController.viewControllers)
    {
        UIViewController *v2=[v.navigationController.viewControllers objectAtIndex:0];
        
        if( [v2 isKindOfClass:[MainViewController class]])
        //if ([v isKindOfClass:[MainViewController class]])
        {
            MainViewController *myViewController = (MainViewController *)v;
            [myViewController setManagedObjectContext:self.managedObjectContext];
        }
        else if ( [v2 isKindOfClass:[MySettingsViewController class]]){
            MySettingsViewController *myViewController = (MySettingsViewController *)v;
            [myViewController setManagedObjectContext:self.managedObjectContext];
            [myViewController setSwimUser:self.swimUser];
            
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if( [segue.identifier isEqualToString:@"SegueMySettingsView"]){
        MySettingsViewController * vc1 =[segue destinationViewController];
        
        vc1.managedObjectContext = self.managedObjectContext;
        vc1.swimUser = self.swimUser;
        
    }
    else if ([segue.identifier isEqualToString:@"SegueMainView"]){
        
    }
    
    //[self.navigationItem setHidesBackButton:false];
}


@end
