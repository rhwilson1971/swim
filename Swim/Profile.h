//
//  Profile.h
//  Swim
//
//  Created by Reuben Wilson on 2/19/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Profile : NSObject
@property NSInteger swimUserId;
@property NSInteger measurementId;
@property NSInteger height;
@property NSInteger weight;
@property NSInteger baseHeartRate;
@property NSInteger age;
@property (nonatomic, retain) NSDate *date;
@property NSInteger sex;
@property NSInteger maxHeartRate;

@end
