//
//  LoginModel.h
//  Swim
//
//  Created by Reuben Wilson on 1/31/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>

enum EAuthMethod {
    UserNamePassword = 1,
    GooglePlus = 2,
    Twitter = 4,
    Facebook = 5,
    Yahoo = 6
    };

@interface LoginModel : NSObject
@property (retain, nonatomic)  NSString* UserName;
@property (retain,nonatomic) NSString* Email;
@property (retain,nonatomic) NSString* FirstName;
@property (retain,nonatomic) NSString* LastName;
@property (retain,nonatomic) NSString* Password;
@property (retain,nonatomic) NSString* Token; // probably should make this the pkey
@property NSInteger sex;
@property NSInteger swimUserId;

@property enum EAuthMethod AuthMethod;
@end
