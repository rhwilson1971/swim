//
//  SwimUserEntity.h
//  Swim
//
//  Created by Reuben Wilson on 4/6/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SwimUserEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * authenticaionType;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSNumber * swimUserId;
@property (nonatomic, retain) NSNumber * isMale;
@property (nonatomic, retain) NSNumber * maxHeartRate;

@end
