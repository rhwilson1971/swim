//
//  MainViewController.m
//  Swim
//
//  Created by Reuben Wilson on 3/2/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
// Say range for heart rate
// Say

#import "MainViewController.h"
#import "PulsingHalo/PulsingHaloLayer.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <AVFoundation/AVFoundation.h>
#import "MyHelpers.h"

#import "BluetoothDeviceEntity.h"
#import <TelerikUI/TelerikUI.h>
#import "SwimDataService.h"
#import "AppDelegate.h"
#import "SwimVoiceCoacher.h"
#import "StepInfo.h"
#import "math.h"

#define HRCharacteristic @"2A29"
#define ChestStrapID @"180D"

@interface MainViewController ()  <CBCentralManagerDelegate, CBPeripheralDelegate>{
    TKChart * _chart;
    
    SwimWorkoutMode _swimWorkoutMode;
    BOOL _isWorkoutStarted;
    
   
    NSInteger _currentHR;
    
    StepInfo * _currentStep;
    SwimDataService *_dataService;
    SwimVoiceChoacher *_swimVoiceCoacher;
    
    NSDate * _startDate;
    BOOL _isPeripheralConnected;
    
    BOOL _shouldGiveInstructions;
    
    NSInteger _workoutCounter;
    BOOL _didStopConnection;
    BOOL _connectionInProgress;
}

@property (nonatomic, strong) NSMutableArray *heartRateMonitors;

@property (nonatomic, strong) CBCentralManager *manager;
@property (nonatomic, strong) CBPeripheral *peripheral;

@property (nonatomic, strong) PulsingHaloLayer *halo;

// heart rate
@property (nonatomic, weak) IBOutlet UIImageView *currentHeartrateView;
@property (nonatomic, weak) IBOutlet UIImageView *targetHeartrateView;

@property (nonatomic, strong) NSString *manufacturerName;

@property (nonatomic, retain) BluetoothDeviceEntity *myBluetoothChestStrap;

@property (nonatomic, weak) IBOutlet UIButton *workoutButton;

@property (nonatomic, weak) IBOutlet UILabel *coachText;
@property (nonatomic, weak) IBOutlet UILabel *strapName;
@property (nonatomic, weak) IBOutlet UILabel *counterLabel;

@property (nonatomic, weak) IBOutlet UILabel *currentHeartrate;
@property (nonatomic, weak) IBOutlet UILabel *targetHeartrate;

@property (nonatomic, weak) IBOutlet UIImageView *coachImage;
@property (nonatomic, weak) IBOutlet UIView *chartView;

//
@property (nonatomic, weak) IBOutlet UISegmentedControl *workoutHRSegControl;

@property NSInteger swimWorkoutSessionId;

@end

@implementation MainViewController
@synthesize heartRateMonitors = _heartRateMonitors;
@synthesize manager = _manager;
@synthesize peripheral = _peripheral;
@synthesize coachText;
@synthesize counterLabel;
@synthesize currentHeartrate;
@synthesize currentHeartrateView;
@synthesize halo;

@synthesize manufacturerName;
@synthesize myBluetoothChestStrap;
@synthesize strapName;
@synthesize targetHeartrate;
@synthesize targetHeartrateView;
@synthesize workoutButton;
@synthesize workoutHRSegControl;

#pragma mark Initialization
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - chart information

- (void)reloadData
{
    [_chart removeAllData];
    
    TKChartStackInfo *stackInfo = nil;
    //if (self.selectedOption < 2) {
        //TKChartStackMode stackMode = self.selectedOption == 0 ? TKChartStackModeStack : TKChartStackModeStack100;
        //stackInfo = [[TKChartStackInfo alloc] initWithID:@(1) withStackMode:stackMode];
    //}
    
    TKChartStackMode stackMode = TKChartStackModeStack;
    stackInfo = [[TKChartStackInfo alloc] initWithID:@(1) withStackMode:stackMode];
    
    for (int i = 0; i < 2; i++) {
        NSMutableArray *points = [[NSMutableArray alloc] init];
        for (int i = 1; i < 4; i++) {
            [points addObject:[[TKChartDataPoint alloc] initWithX:@(i) Y:@(arc4random() % 100)]];
        }
        
        TKChartSeries *series = [[TKChartColumnSeries alloc] initWithItems:points];
        
        series.title = [NSString stringWithFormat:@"Series %d", i];
        series.stackInfo = stackInfo;
        series.selectionMode = TKChartSeriesSelectionModeSeries;
        [_chart addSeries:series];
    }
    
    [_chart reloadData];
}

#pragma mark - View Conroller

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.peripheral = nil;
    
    _swimWorkoutMode = SwimModeNone;
    _didStopConnection = NO;
    
    // set to workout
    [workoutHRSegControl setSelectedSegmentIndex:0];
    
    UIDevice * device = [UIDevice currentDevice];
    BOOL backgroundSupported = NO;
    
    if( [device respondsToSelector:@selector(isMultitaskingSupported)])
        backgroundSupported = device.multitaskingSupported;
    
    _workoutCounter = 0;
    _chartView.hidden = YES;
    
    _isPeripheralConnected = NO;

    self.targetHeartrate.text = @"0";
    self.myBluetoothChestStrap = nil;

    _dataService = [[SwimDataService alloc] initWithServerAddress:@"ec2-54-200-147-89.us-west-2.compute.amazonaws.com"];
    //_isWorkoutStarted = NO;
    
    self.navigationController.navigationBar.barTintColor = [MyHelpers swimDarkColor];
    self.navigationController.navigationBar.translucent = NO;
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    [self initializeAudioSession];
    
    self.managedObjectContext = appDelegate.managedObjectContext;
    
    _chart = [[TKChart alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    _chart.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [_chartView addSubview:_chart];
    
    [self reloadData];
    
    self.view.backgroundColor =  [MyHelpers swimLightColor];
    
    
    // user information
    _currentUser = [self getUser];
    
    NSInteger maxHR = [_currentUser.maxHeartRate intValue];
    
    // voice coacher
    _swimVoiceCoacher = [[SwimVoiceChoacher alloc] initWithMaxHeartRate:(NSInteger)maxHR];
    [_swimVoiceCoacher setupSteps];
    
    
    //[_swimVoiceCoacher addReading:107 ];
    
    if( [_currentUser.isMale intValue] == 0){
        self.coachImage.image = [UIImage imageNamed:@"coach-male-64x64.png"];
    }
    
    // coach text
    [coachText setAdjustsFontSizeToFitWidth:NO];
    [coachText setNumberOfLines:0];
    coachText.text = @"Hi, I'm your SWiM coach, put on your chest strap so we can get started";
    
    // start / stop / pause workout button
    workoutButton.layer.cornerRadius = 3;
    workoutButton.backgroundColor = [MyHelpers swimMediumColor];
    workoutButton.layer.borderColor = [MyHelpers swimDarkColor].CGColor;
    workoutButton.layer.borderWidth =1;
    workoutButton.tintColor = [MyHelpers swimLightColor];
    workoutButton.titleLabel.text = @"Start";
    [workoutButton setTitle:@"Start" forState:UIControlStateNormal];
    workoutButton.layer.cornerRadius = workoutButton.bounds.size.width /2.0;
    
    // counter label
    counterLabel.textColor = [MyHelpers swimDarkColor];
    strapName.textColor = [MyHelpers swimDarkColor];
    
    // Do any additional setup after loading the view.
    self.strapName.text = @"No Cheststrap detected";
    
    self.currentHeartrate.font = [UIFont boldSystemFontOfSize:20.0];
    self.currentHeartrate.text = @"";
    self.currentHeartrate.autoresizingMask = UIViewAutoresizingFlexibleWidth+UIViewAutoresizingFlexibleHeight;
    
    //self.currentHeartrate.textAlignment = UITextAlignmentCenter;
    self.currentHeartrate.backgroundColor = [UIColor clearColor];
    self.currentHeartrate.textColor = [UIColor redColor];
    self.currentHeartrate.text = @"0";
    
    self.halo = [PulsingHaloLayer layer];
    self.halo.position = self.currentHeartrateView.center;
    [self.view.layer insertSublayer:self.halo below:self.currentHeartrateView.layer];
    
    // [self setupInitialValues];
    // [self changeConnectionStatus:2];
    
    
    /// bluetooth tracking
    self.heartRateMonitors = [NSMutableArray array];
    
    
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:nil];
    
    
    
    // don't automatically scan
    //[self startScan];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait ; //| UIInterfaceOrientationMaskLandscapeLeft;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


#pragma mark - Start/Stop Scan methods

// Use CBCentralManager to check whether the current platform/hardware supports Bluetooth LE.
- (BOOL) isLECapableHardware
{
    BOOL retVal = FALSE;
    
    NSString * state = nil;
    switch ([self.manager state]) {
        case CBCentralManagerStateUnsupported:
            state = @"The platform/hardware doesn't support Bluetooth Low Energy.";
            break;
        case CBCentralManagerStateUnauthorized:
            state = @"The app is not authorized to use Bluetooth Low Energy.";
            break;
        case CBCentralManagerStatePoweredOff:
            state = @"Bluetooth is currently powered off.";
            break;
        case CBCentralManagerStatePoweredOn:
            state = @"Bluetooth is currently powered on";
            break;
            //return TRUE;
            retVal = TRUE;
        case CBCentralManagerStateUnknown:
        default:
            //return FALSE;
            state=@"Unknown";
            break;
    }
    NSLog(@"Central manager state: %@", state);
    return retVal;
}

// Request CBCentralManager to scan for heart rate peripherals using service UUID 0x180D
- (void) startScan
{
    NSLog(@"Starting periphial scan");
    
    CBUUID *heartRateServiceUUID = [CBUUID UUIDWithString:@"180D"];
    
 
    [self.manager scanForPeripheralsWithServices:[NSArray arrayWithObject:heartRateServiceUUID] options:nil];
}

// Request CBCentralManager to stop scanning for heart rate peripherals
- (void) stopScan
{
    NSLog(@"Stopping periphial scan");
    [self.manager stopScan];
}

#pragma mark - CBCentralManager delegate methods
// Invoked when the central manager's state is updated.
- (void) centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSLog(@"State is being updated");
    [self isLECapableHardware];
}

// Invoked when the central discovers heart rate peripheral while scanning.
- (void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    NSLog(@"didDiscoverPeripheral ");
    NSMutableArray *peripherals = [self mutableArrayValueForKey:@"heartRateMonitors"];
    
    if(![self.heartRateMonitors containsObject:aPeripheral])
    {
        [peripherals addObject:aPeripheral];
        //[self changeConnectionStatus:3];
    }
    
    // Retrieve already known devices
    //CBUUID *cbUUID = [CBUUID UUIDWithNSUUID:aPeripheral.identifier];
    
    NSArray *connectedPeripheralArray = [self.manager retrievePeripheralsWithIdentifiers:[NSArray arrayWithObject:aPeripheral.identifier]];
    
    NSLog(@"Count %lu", (unsigned long)[connectedPeripheralArray count]);
    
    [self.manager connectPeripheral:aPeripheral options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:CBConnectPeripheralOptionNotifyOnConnectionKey]];
    
    
    NSLog(@"Connecting to peripheral");
    
    //if( [connectedPeripheralArray count] > 0){
    //    [self.manager connectPeripheral:aPeripheral options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:CBConnectPeripheralOptionNotifyOnDisconnectionKey]];
    //}
    
    //[self.manager retrievePeripherals:[NSArray arrayWithObject:(id)aPeripheral.UUID]];
}

// Invoked when the central manager retrieves the list of known peripherals.
// Automatically connect to first known peripheral
- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals
{
    NSLog(@"Retrieved peripheral: %lu - %@", (unsigned long)[peripherals count], peripherals);
    [self stopScan];
    
    // If there are any known devices, automatically connect to it.
    if([peripherals count] >= 1)
    {
        self.peripheral = [peripherals objectAtIndex:0];
        [self.manager
         connectPeripheral:self.peripheral
         options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:CBConnectPeripheralOptionNotifyOnNotificationKey]];
    }
}

// Invoked when a connection is succesfully created with the peripheral.
// Discover available services on the peripheral
- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    NSLog(@"connected");

    _isPeripheralConnected = YES;
    _connectionInProgress = NO;
    // [self stopRetryTimer];
    
    self.peripheral = aPeripheral;
    [self.peripheral setDelegate:self];
    [self.peripheral discoverServices:nil];
}

// Invoked when an existing connection with the peripheral is torn down.
// Reset local variables
- (void) centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    NSLog(@"disconnected");
    
    if( _didStopConnection == NO )
        self.strapName.text = @"Strap disconnected, looking...";
    
    _isPeripheralConnected = NO;
    
    if (self.peripheral) {
        [self.peripheral setDelegate:nil];
        self.peripheral = nil;
    }
    
    NSLog(@"Peripheral disconnected");
    
    if( _didStopConnection == NO )
        [self startScan];
    
    // [self startRetryTimer];
}

// Invoked when the central manager fails to create a connection with the peripheral.
- (void) centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    NSLog(@"Fail to connect to peripheral: %@ with error = %@", aPeripheral, [error localizedDescription]);
    // [self changeConnectionStatus:2];
    
    if (self.peripheral) {
        [self.peripheral setDelegate:nil];
        self.peripheral = nil;
    }
}

#pragma mark - timer methods
/*
-(void) startRetryTimer{
    _deviceConnectionTimer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(retryConnectionMethod:) userInfo:nil repeats:NO];
}

-(void) stopRetryTimer {
    _deviceConnectionTimer = nil;
}

-(void)retryConnectionMethod:(NSTimer *)timer{

    if( timer == _deviceConnectionTimer){

        _deviceConnectionTimer = nil;

        NSLog(@"Connection timer expired, Restarting peripheral scan");

        if( _isPeripheralConnected == NO)
            [self startScan];   
    }
}
*/

#pragma mark - CBPeripheral delegate methods

// Invoked upon completion of a -[discoverServices:] request.
// Discover available characteristics on interested services
- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverServices:(NSError *)error
{
    NSLog(@"didDiscoverServices");
    
    for (CBService *aService in aPeripheral.services) {
        NSLog(@"Service found with UUID: %@", aService.UUID);
        
        /* Heart Rate Service */
        if ([aService.UUID isEqual:[CBUUID UUIDWithString:@"180D"]]) {
            [aPeripheral discoverCharacteristics:nil forService:aService];
        }
        
        /* Device Information Service */
        if ([aService.UUID isEqual:[CBUUID UUIDWithString:@"180A"]]) {
            [aPeripheral discoverCharacteristics:nil forService:aService];
        }
        
        /* GAP (Generic Access Profile) for Device Name */
        if ([aService.UUID isEqual:[CBUUID UUIDWithString:CBUUIDGenericAccessProfileString]]) {
            [aPeripheral discoverCharacteristics:nil forService:aService];
        }
    }
}

// Invoked upon completion of a -[discoverCharacteristics:forService:] request.
// Perform appropriate operations on interested characteristics
- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    NSLog(@"disDiscoverCharactersticsForService");
    
    if ([service.UUID isEqual:[CBUUID UUIDWithString:@"180D"]]) {
        for (CBCharacteristic *aChar in service.characteristics) {
            // Set notification on heart rate measurement
            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:@"2A37"]]) {
                [self.peripheral setNotifyValue:YES forCharacteristic:aChar];
                NSLog(@"Found a Heart Rate Measurement Characteristic");
            }
            
            // Read body sensor location
            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:@"2A38"]]) {
                [aPeripheral readValueForCharacteristic:aChar];
                NSLog(@"Found a Body Sensor Location Characteristic");
            }
            
            // Write heart rate control point
            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:@"2A39"]]) {
                uint8_t val = 1;
                NSData* valData = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
                [aPeripheral writeValue:valData forCharacteristic:aChar type:CBCharacteristicWriteWithResponse];
            }
        }
    }
    
    if ([service.UUID isEqual:[CBUUID UUIDWithString:CBUUIDGenericAccessProfileString]]) {
        for (CBCharacteristic *aChar in service.characteristics) {
            // Read device name
            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:CBUUIDDeviceNameString]]) {
                [aPeripheral readValueForCharacteristic:aChar];
                NSLog(@"Found a Device Name Characteristic");
            }
        }
    }
    
    if ([service.UUID isEqual:[CBUUID UUIDWithString:@"180A"]]) {
        for (CBCharacteristic *aChar in service.characteristics) {
            // Read manufacturer name
            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:@"2A29"]]) {
                [aPeripheral readValueForCharacteristic:aChar];
                NSLog(@"Found a Device Manufacturer Name Characteristic");
            }
        }
    }
}

// Update UI with heart rate data received from device
- (void) updateWithHRMData:(NSData *)data
{
    NSLog(@"updateWithHRMData");
    const uint8_t *reportData = [data bytes];
    uint16_t bpm = 0;
    
    if ((reportData[0] & 0x01) == 0) {
        // uint8 bpm
        bpm = reportData[1];
    } else {
        // uint16 bpm
        bpm = CFSwapInt16LittleToHost(*(uint16_t *)(&reportData[1]));
    }
    NSLog(@"bpm %d", bpm);
    
    self.currentHeartrate.text = [NSString stringWithFormat:@"%d", bpm];

    _currentHR = bpm;
 
    //NSLog(@"The result is %f", result);

    //updateInstructionsBlock();

    // coach user if workout has started, return if not
    // [self coachWhileWorkingOut:_currentHR];
    
    if( _swimWorkoutMode == SwimModeNone )
    {
        [self runSelectedSwimOption];
    }
    else if ( _swimWorkoutMode == SwimModeWorkout )
    {
        [self coachWhileWorkingOut:_currentHR];
    }
    // swim mode heartrate just keep it moving
}

// Invoked upon completion of a -[readValueForCharacteristic:] request
// or on the reception of a notification/indication.
- (void) peripheral:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic
              error:(NSError *)error
{
    NSLog(@"didUpdateValueForCharacterstics");
    
    // Updated value for heart rate measurement received
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A37"]]) {
        if(characteristic.value || !error) {
            NSLog(@"received value: %@", characteristic.value);
            // Update UI with heart rate data
            [self updateWithHRMData:characteristic.value];
        }
    }
    // Value for body sensor location received
    else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A38"]]) {
        NSData * updatedValue = characteristic.value;
        uint8_t* dataPointer = (uint8_t*)[updatedValue bytes];
        if (dataPointer) {
            uint8_t location = dataPointer[0];
            NSString*  locationString;
            switch (location) {
                case 0:
                    locationString = @"Other";
                    break;
                case 1:
                    locationString = @"Chest";
                    break;
                case 2:
                    locationString = @"Wrist";
                    break;
                case 3:
                    locationString = @"Finger";
                    break;
                case 4:
                    locationString = @"Hand";
                    break;
                case 5:
                    locationString = @"Ear Lobe";
                    break;
                case 6:
                    locationString = @"Foot";
                    break;
                default:
                    locationString = @"Reserved";
                    break;
            }
            NSLog(@"Body Sensor Location = %@ (%d)", locationString, location);
        }
    }
    // Value for device Name received
    else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:CBUUIDDeviceNameString]]) {
        NSString * deviceName = [[NSString alloc] initWithData:characteristic.value
                                                      encoding:NSUTF8StringEncoding];
        NSLog(@"Device Name = %@", deviceName);
    }
    // Value for manufacturer name received
    else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A29"]]) {
        NSString *manufacturer = [[NSString alloc] initWithData:characteristic.value
                                                       encoding:NSUTF8StringEncoding];
        NSLog(@"Manufacturer Name = %@", manufacturer);
        manufacturerName = manufacturer;
        // self.myBluetoothChestStrap.manufacturer = manufacturer;
        
        self.strapName.text = manufacturerName;
        
        NSUUID *identifier = aPeripheral.identifier;
        NSString *deviceId = [identifier UUIDString];
        
        [self saveStrapInfo:@"MyID" withManufacturer:manufacturer withDeviceId:deviceId];
        
        // [self changeConnectionStatus:1];
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    NSLog(@"notification updated for charecteristic");
}


- (void)cleanupBluetoothConnection {
    
    // See if we are subscribed to a characteristic on the peripheral
    if (self.peripheral.services != nil)
    {
        for (CBService *service in self.peripheral.services)
        {
            if (service.characteristics != nil)
            {
                for (CBCharacteristic *characteristic in service.characteristics)
                {
                    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A37"]])
                    {
                        if (characteristic.isNotifying)
                        {
                            [self.peripheral setNotifyValue:NO forCharacteristic:characteristic];
                            break;
                        }
                    }
                }
            }
        }
    }
    
    //[self.peripheral ]
    
    [self.manager cancelPeripheralConnection:self.peripheral];
}

-(void) reconnectToDisconnectedDevice{
    if(nil != self.peripheral){
        //[self.manager retrievePeripheralsWithIdentifiers:
        // [NSArray arrayWithObject:self.peripheral.identifier]];
        
        NSLog(@"Reconnecting Peripheral");
        [self.manager connectPeripheral:self.peripheral options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:CBConnectPeripheralOptionNotifyOnConnectionKey]];
    }
}

#pragma mark - Voice Coaching

-(void) voiceCoachMessage:(NSString *)quote{
    
    NSLog(@"Coaching user");
    AVSpeechSynthesizer *synthesizer = [[AVSpeechSynthesizer alloc] init];

    AVSpeechUtterance * utterance = nil;
    
    utterance = [AVSpeechUtterance speechUtteranceWithString:quote];
    
    [utterance setRate:0.2f];
    
    [synthesizer speakUtterance:utterance];
    
    NSLog(@"%f", AVSpeechUtteranceDefaultSpeechRate);
    
}

#pragma mark - Data methods

-(void) changeConnectionStatus:(NSInteger) connectionStatus{
    
    switch( connectionStatus)
    {
        case 1:
            //connectChestStrap.titleLabel.text = manufacturerName;
            break;
            
        case 2:
            //connectChestStrap.titleLabel.text = @"Disconnected";
            break;
            
        case 3:
            //connectChestStrap.titleLabel.text = @"Connecting...";
            break;
    }
}

-(void) getExistingBluetoothChestStrap{
    NSLog(@"Get existing user chest strap from mysql");
    NSManagedObjectContext * moc = self.managedObjectContext;
    
    if(nil == moc) return;
    
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"BluetoothDeviceEntity" inManagedObjectContext:moc];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entityDescription];
    NSError *error;
    NSArray *items = [moc executeFetchRequest:request error:&error];
    
    if(nil != items && [items count] > 0)
    {

        
    }
    else
    {
        self.myBluetoothChestStrap = [[BluetoothDeviceEntity alloc] init];
        self.myBluetoothChestStrap.myName = [NSString stringWithFormat:@"%@'s Strap", self.currentUser.firstName];
    }
}

-(void) addFoundChestrapToAvaiableList:(NSString *)manufacturer withUniqueId:(NSString *)uniqueId withStrapName:(NSString *)strapName{
}


-(void) saveStrapInfo:(NSString *)myName withManufacturer:(NSString *)manufacturer withDeviceId:(NSString *)deviceId{
    NSManagedObjectContext *moc = self.managedObjectContext;
    
    if(nil != moc) {
        
        BluetoothDeviceEntity *newStrap = (BluetoothDeviceEntity *)[NSEntityDescription insertNewObjectForEntityForName:@"BluetoothDeviceEntity" inManagedObjectContext:moc];
        
        newStrap.myName = myName;
        newStrap.datePaired = [NSDate date];
        newStrap.manufacturer = manufacturer;
        newStrap.deviceId= deviceId;
        newStrap.primaryDevice = [NSNumber numberWithInteger:1];
        
        NSError *error=nil;
        
        [moc save:&error];
        
        if(nil != error){
            NSLog(@"Error saving %@", error.description);
        }
        else{
            self.myBluetoothChestStrap = newStrap;
        }
    }
}

-(SwimUserEntity *) getUser{

    SwimUserEntity *swimUser = nil;

    NSManagedObjectContext * moc = self.managedObjectContext;
    
    if(nil == moc) return swimUser;
    
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"SwimUserEntity" inManagedObjectContext:moc];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entityDescription];
    NSError *error;
    
    NSArray *items = [moc executeFetchRequest:request error:&error];
    
    if(nil != items && [items count] > 0)
    {
        swimUser = [items objectAtIndex:0];
    }
    
    return swimUser;
}

#pragma mark - Workout methods
-(void) coachWhileWorkingOut:(NSInteger )heartRate{
    
    if( _swimWorkoutMode != SwimModeWorkout)
    {
        NSLog(@"workout not started, no coaching needed");
        return ;
    }
    
    NSLog(@"Check coaching interval");
    NSDate *endDate = [NSDate date];
    
    NSTimeInterval interval =[endDate timeIntervalSinceDate:_startDate];
    
    if( interval >= _currentStep.interval){
        
        _startDate = [NSDate date]; // reset start date reference
        _currentStep = [_swimVoiceCoacher nextStep];
        
        if(nil == _currentStep){
            
            self.workoutButton.titleLabel.text = @"Start";
            [workoutButton setTitle:@"Start" forState:UIControlStateNormal];
            
            NSString *instructions = @"Good Job!  You've completed your workout";
            [self voiceCoachMessage:instructions];
            self.coachText.text = instructions;
            
            return ;
        }
        
        NSLog(@"Current step is %d", (int)_currentStep.currentStep);
        
        NSInteger nextStepMaxHeartRate = _currentStep.maxTargetHeartRate;
        NSInteger nextStepMinHeartRate = _currentStep.minTargetHeartRate;
        
        self.targetHeartrate.text = [NSString stringWithFormat:@"%ld - %ld", (long)nextStepMaxHeartRate,
                                     nextStepMinHeartRate];
        
        // NSString *instruction = [NSString stringWithFormat:@"Your heart rate is %d.  For the next 3 minutes try to target your heart rate between %d and %d", (int)_currentHR, (int)nextStepMinHeartRate, (int)nextStepMaxHeartRate];
        
        NSLog(@"current delta %f interval %f" , interval, _currentStep.interval);
        
        
        // [self voiceCoachMessage:instruction];

        void (^getInstruction) (uint16_t) ;
        
        getInstruction = ^(uint16_t heartRate) {

            [_dataService saveHeartrateWithUserId:[_currentUser.swimUserId intValue] withHeartrate:_currentHR];
                                      
            NSLog(@"Block executed heart rate %d", heartRate);
            // return [_swimVoiceCoacher intructtions:bpm, _currentUser.maxHeartRate,  ]

            NSString *instruction = [_swimVoiceCoacher instructionWithHeartRate:heartRate withUserId:[_currentUser.swimUserId intValue] withSessionId:self.swimWorkoutSessionId];

                if(nil != instruction){
                    [self voiceCoachMessage:instruction];
                }
            return;
            };
     
        // sgetInstruction(heartRate);
    }
    
    // NSInteger currentTimeInterval = (NSInteger) interval;
    
    //if( (currentTimeInterval % 5) == 0 ){  // save heartrate while active every 5 seconds
    //    NSLog(@"save data service");
    //    [_dataService saveHeartrateWithUserId:[_currentUser.swimUserId intValue] withHeartrate:_currentHR];
    //}
    
    //if( (currentTimeInterval % 30) == 0 && currentTimeInterval > 0){
    //    
    //    NSString *instruction = [NSString stringWithFormat:@"Your heart rate is %d.  The current target heart rate is between %d and %d",
    //                             (int)_currentHR,
    //                             (int)_currentStep.minTargetHeartRate,
    //                             (int)_currentStep.maxTargetHeartRate ];
    //    
    //    // [self voiceCoachMessage:instruction];
    //}
    
    [self updateWorkoutTime];

}

-(void)startWorkout{
    
    if(NO == _isPeripheralConnected )
        return ;
    
    // kill the workout sesion on the server
    self.swimWorkoutSessionId = [_dataService saveSessionWithUserId:[_currentUser.swimUserId intValue] ];

    _swimWorkoutMode = SwimModeWorkout;
        
    // Get first leg of workout
    _currentStep = [_swimVoiceCoacher nextStep];
        
    self.targetHeartrate.text = [NSString stringWithFormat:@"%ld - %ld", (long)_currentStep.minTargetHeartRate, (long)_currentStep.maxTargetHeartRate];
        
    _startDate = [NSDate date];
        
    //_isWorkoutStarted = YES;
        
    NSString * instruction = @"Try to reach and maintain target heart rate, okay?";
    [self voiceCoachMessage:instruction];
    coachText.text = instruction;
        
    [workoutButton setTitle:@"Stop" forState:UIControlStateNormal];
        
    /*
     } else {
        
        _startDate = nil;
        _isWorkoutStarted = NO;
        // _workoutTimer = nil;
        
        coachText.text = @"Hit start to start another workout";
        
        [workoutButton setTitle:@"Start" forState:UIControlStateNormal];
        counterLabel.text = @"00:00:00";
        
        [_swimVoiceCoacher resetSteps];
    }
    */
    NSLog(@"Started workout");

}

-(void)stopWorkout{
    
    
    NSInteger tempId = [_dataService saveSessionWithUserId:[_currentUser.swimUserId intValue] withExistingSessionId:self.swimWorkoutSessionId];

    NSLog(@"Workout session id = %ld", tempId);
    
    _startDate = nil;
    _swimWorkoutMode = SwimModeNone;
    
    coachText.text = @"Hit start to start another workout";
    
    [workoutButton setTitle:@"Start" forState:UIControlStateNormal];
    counterLabel.text = @"00:00:00";
    
    [_swimVoiceCoacher resetSteps];
}

-(void)startHeartrateMonitor{
    _swimWorkoutMode = SwimModeMonitor;
    
    [workoutButton setTitle:@"Stop" forState:UIControlStateNormal];
}

-(IBAction)start:(id)sender{
    
    if( _connectionInProgress == YES )
        return ;

    if( _swimWorkoutMode != SwimModeNone ){
        
        _didStopConnection = YES;
        [self cleanupBluetoothConnection];
        [self stopWorkout];
        
    }
    else {
        _connectionInProgress = YES;
        _didStopConnection = NO;
        [self startScan];
    }
}

-(void)runSelectedSwimOption{
    NSInteger swimOption = [workoutHRSegControl selectedSegmentIndex];
    
    switch(swimOption){
        case 0:
        {
            [self startHeartrateMonitor];
            break;
        }
            
        case 1:
        {
            [self startWorkout];
            break;
        }
    }
}

-(void) updateWorkoutTime{

    _workoutCounter ++;
    int seconds = _workoutCounter % 60;
    int minutes = (int)_workoutCounter / 60;
    
    counterLabel.text = [NSString stringWithFormat:@"%0.2d:%0.2d", minutes, seconds];
}

-(IBAction)pauseWorkout:(id)sender{
    
}

-(void) initializeAudioSession{
    // [[AVAudioSession sharedInstance] setDelegate:self];

}
@end
