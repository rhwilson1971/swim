//
//  ExercisesViewController.h
//  Swim
//
//  Created by Reuben Wilson on 1/29/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExercisesViewController : UITableViewController

@end
