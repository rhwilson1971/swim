//
//  MyHelpers.m
//  Swim
//
//  Created by Reuben Wilson on 3/18/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "MyHelpers.h"

@implementation MyHelpers


+(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+(UIColor *) swimMediumColor {
    
    //return [MyHelpers colorFromHexString:@"01B6AD"];
    
    return [UIColor colorWithRed:0.004 green:0.714 blue:0.678 alpha:1.0];
}

+(UIColor *) swimDarkColor {
    // return [MyHelpers colorFromHexString:@"0A4958"];
    
    return [UIColor colorWithRed:0.039 green:0.286 blue:0.345 alpha:1.0];
}

+(UIColor *)swimLightColor {
    // return [MyHelpers colorFromHexString:@"F6E7D2"];

    return [UIColor colorWithRed:0.965 green:0.906 blue:0.824 alpha:1.0];
}

@end

