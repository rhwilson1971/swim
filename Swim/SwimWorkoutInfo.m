#import "SwimWorkoutInfo.h"

@interface SwimWorkoutInfo ()

@end

@implementation SwimWorkoutInfo

@synthesize step;
@synthesize  interval;
@synthesize  hrMinFactor;
@synthesize  hrMaxFactor;
@synthesize  hrAvgFactor;
@synthesize  hrActualMin;
@synthesize  hrActualMax;
@synthesize  hrActualAvg;
@synthesize  start;
@synthesize  end ;
@synthesize timeSegment;
@synthesize  timeAccumulated;

@end


