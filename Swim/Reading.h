//
//  Reading.h
//  Swim
//
//  Created by Reuben Wilson on 4/22/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Reading : NSObject

@property double elapsedTime;
@property double maxHeartRate;
@property NSInteger heartRate;

@property double hrPercentOfMax;
@property double hrPercentOfMaxDelta; // need 2 heart rates
@property double hrThreeReadingAverage;
@property double predictedReading;
@property double targetPercent; // StepInfo
@property double startingHRPercent;
@property double originalTimed;
@property double targetRange3ReadingAvg;
@property double targetRangeRunningAvg;
@property double startingP_ie1;



@property double p_lessThanTarget_3ReadingAvg;
@property double p_lessThanTarget_runningAvg;
@property double startingP_lessThanTarget;

@property double p_greaterThanTarget_3ReadingAvg;
@property double p_greaterThanTarget_runningAvg;

@property double startingP_greaterThanTarget;

@property double h_ie2a;
@property double h_s2a;
@property double h_ToT;

@property double p_ie1;
@property double p_s2;

@property double h_s2b;
@property double h_pow2_s2;

@property double h_ie2b;
@property double h_pow2_ie2;

@property double s1;
@property double e2;
@property double d1a;
@property double d1a_pow2;
@property double t1a;
@property double t1a_pow2;
@property double managementIndex;

@property double pe1;
@property double ps1;
@property double he1;
@property double he1_pow2;
@property double hs1;
@property double hs1_pow2;

@property double d1b;
@property double d1b_pow2;

@property double t1b;
@property double t1b_pow2;

@property double trendIndex;

-(Reading *) initWithMaxHeartRate:(NSInteger) currentHeartRate;


@end
