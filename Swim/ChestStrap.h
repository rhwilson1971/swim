//
//  ChestStrap.h
//  Swim
//
//  Created by Reuben Wilson on 4/3/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChestStrap : NSObject

@property (nonatomic,retain) NSString *name;
@property (nonatomic,retain) NSString *uniqueId;
@property (nonatomic,retain) NSString *brand;
@property BOOL isPrimary;

@end
