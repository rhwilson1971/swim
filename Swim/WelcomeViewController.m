//
//  WelcomeViewController.m
//  Swim
//
//  Created by Reuben Wilson on 1/27/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import "WelcomeViewController.h"
#import "AppDelegate.h"
#import "SwimUserEntity.h"
#import "NewUserViewController.h"
#import "MainViewTabController.h"
#import "LoginViewController.h"
#import "MyHelpers.h"
#import <QuartzCore/QuartzCore.h>

@interface WelcomeViewController ()
@property (nonatomic,weak) IBOutlet UIButton * loginButton;
@property (nonatomic,weak) IBOutlet UIButton * signUpButton;
@property (nonatomic,weak) IBOutlet UILabel * swimMessage;
@property (nonatomic,weak) IBOutlet UIView *signInView;
@end

@implementation WelcomeViewController
@synthesize managedObjectContext;
@synthesize signUpButton;
@synthesize loginButton;
@synthesize swimMessage;
@synthesize signInView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    /*
    UIView *logoImage = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 640)];
    logoImage.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"SWiM-LaunchImage.png"]];
    [logoImage.layer setOpaque:NO];
    logoImage.opaque = NO;
    
    
    [self.view addSubview:logoImage];
    */
    
    
    self.view.backgroundColor =  [MyHelpers swimDarkColor];
    
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"LaunchImage640x1136.png"]];
    //logoImage = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 56)];
    //logoImage.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Logo.png"]];
    //[logoImage.layer setOpaque:NO];
    //logoImage.opaque = NO;
    
    
    //[self.view addSubview:logoImage];
    
    // self.navigationController.view.hidden = true;
    // [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    [super viewDidLoad];
    
    signInView.layer.cornerRadius = 4;
    signInView.backgroundColor = [MyHelpers swimMediumColor];
    
    signUpButton.backgroundColor = [MyHelpers swimLightColor];
    signUpButton.tintColor = [MyHelpers swimDarkColor];
    signUpButton.layer.cornerRadius = 3;
    
    loginButton.backgroundColor = [MyHelpers swimLightColor];
    loginButton.tintColor = [MyHelpers swimDarkColor];
    loginButton.layer.cornerRadius = 3;
    
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    
    swimMessage.textColor = [MyHelpers swimDarkColor];
    
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        self.navigationController.navigationBar.barTintColor = [MyHelpers swimMediumColor];
        self.navigationController.navigationBar.translucent = NO;
    }else {
        self.navigationController.navigationBar.tintColor = [MyHelpers swimMediumColor];
    }
    
    //self.navigationController.tabBarController.
    // self.view.backgroundColor =  [MyHelpers swimDarkColor];
    
    self.navigationItem.hidesBackButton = true;
    // Do any additional setup after loading the view.
    
    NSManagedObjectContext *moc = self.managedObjectContext;
    
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"SwimUserEntity" inManagedObjectContext:moc];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entityDescription];
    NSError *error;
    
    NSArray *items = [moc executeFetchRequest:request error:&error];
    
    if(nil != items && [items count] > 0)
    {
        NSArray * users = [[NSMutableArray alloc] initWithArray:items];
        
        SwimUserEntity * swimUser = [users objectAtIndex:0];
        
        NSLog(@"%@", [swimUser email]);
        
        [self performSegueWithIdentifier:@"MainViewSeque" sender:self];
    }
    
}

- (NSUInteger)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait ; //| UIInterfaceOrientationMaskLandscapeLeft;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{    
    NSLog(@"Hello");
    
    NSString * sid =[segue identifier];
    NSLog(@"%@", sid);
    
    if([sid isEqualToString:@"NewUserSegue"]){
        
        NewUserViewController *vc = (NewUserViewController *)[segue destinationViewController];
        
        vc.managedObjectContext = self.managedObjectContext;
        // vc.loginModel = self.loginModel;
    }
    else if( [sid isEqualToString:@"MainViewSeque"]){
        MainViewTabController * vc = (MainViewTabController *)[segue destinationViewController];
        vc.managedObjectContext = self.managedObjectContext;
    }
    else if( [sid isEqualToString:@"LoginUserSegue"]){
        LoginViewController *vc = (LoginViewController *)[segue destinationViewController];
        vc.managedObjectContext = self.managedObjectContext;
    }
}


@end
