//
//  PageContentViewController.h
//  Swim
//
//  Created by Reuben Wilson on 2/21/14.
//  Copyright (c) 2014 Thomas Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;

@end
